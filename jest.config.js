// jest.config.js
// const { defaults: tsjPreset } = require('ts-jest/presets')

// module.exports = {
//   ...tsjPreset,
//   cacheDirectory: '.jest/cache',
//   preset: 'react-native',
//   testPathIgnorePatterns: ['\\.snap$', '<rootDir>/node_modules/'],
//   testRegex: '(/__tests__/.*|\\.(test|spec))\\.(ts|tsx|js)$',
//   transform: {
//     ...tsjPreset.transform,
//     '\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
//   },
// }

module.exports = {
  preset: 'jest-expo',
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/react-native/jest/preprocessor.js',
    '^.+\\.tsx?$': 'ts-jest',
  },
  testMatch: ['**/__tests__/**/*.ts?(x)', '**/?(*.)+(spec|test).ts?(x)'],
  moduleFileExtensions: ['js', 'ts', 'tsx'],
  globals: {
    'ts-jest': {
      tsConfig: {
        jsx: 'react',
      },
    },
  },
}
