import React from 'react'
// import { StatusBar } from 'react-native'

// import withHooks from 'react-with-hooks'
import { compose } from 'recompose'
// import { ThemeContext as StyledContext, withTheme } from 'styled-components'

// import { withActionSheetProvider } from './hoc/withActionSheetProvider'
// import { withApolloProvider } from './hoc/withApolloProvider'
import { withAppLoader } from './src/hoc/withAppLoader'
// import { withFirebaseService } from './hoc/withFirebaseService'
// import { withFlashMessage } from './hoc/withFlashMessage'
// import { withI18nProvider } from './hoc/withI18nProvider'
// import { withPaperProvider } from './hoc/withPaperProvider'
// import { withThemeProvider } from './hoc/withThemeProvider'

import { Spinner } from './src/components'
import { RootNavigator } from './src/navigation/RootNavigator'

// import { ThemeContext } from './context/ThemeContext'
import { setTopLevelNavigator } from './src/services/Navigation'

// import { withAlertProvider } from './hoc/withAlertProvider'
// import { useContext } from './hooks'
// import { Theme } from './system'

const Root = () => {
  // const theme = useContext<Theme>(StyledContext)
  // const { themeMode } = useContext(ThemeContext)
  return (
    <React.Fragment>
      {/* <StatusBar barStyle={themeMode === 'dark' ? 'light-content' : 'dark-content'} /> */}
      <RootNavigator
        screenProps={
          {
            // theme,
          }
        }
        renderLoadingExperimental={() => <Spinner />}
        ref={setTopLevelNavigator}
        persistenceKey={process.env.NODE_ENV !== 'production' ? 'NavigationStateDEV' : null}
      />
    </React.Fragment>
  )
}

Root.displayName = 'Root'

const App = compose(
  // withHooks,
  withAppLoader,
  // withFirebaseService,
  // withApolloProvider,
  // withThemeProvider,
  // withPaperProvider,
  // withActionSheetProvider,
  // withI18nProvider,
  // withTheme,
  // withFlashMessage,
  // withAlertProvider,
)(Root)

export default App
