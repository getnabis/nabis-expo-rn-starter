import * as React from 'react'
import { Animated } from 'react-native'

import { Box, Color } from '../system'
import { Icon } from './Icon'

interface Props {
  backgroundColor: Color
  borderColor: Color
  selectedBackgroundColor: Color
  selectedBorderColor: Color
  selected: boolean
  iconColor: Color
}

interface State {
  scaleCheckmarkValue: Animated.Value
}

export class RadioInput extends React.Component<Props, State> {
  state = {
    scaleCheckmarkValue: new Animated.Value(0),
  }

  scaleCheckmark = (value: number) => {
    Animated.timing(this.state.scaleCheckmarkValue, {
      toValue: value,
      duration: 400,
      // easing: Easing.easeOutBack,
    }).start()
  }

  render() {
    const {
      selected,
      iconColor,
      selectedBackgroundColor,
      selectedBorderColor,
      backgroundColor,
      borderColor,
    } = this.props

    const background = selected ? selectedBackgroundColor : backgroundColor

    const border = selected ? selectedBorderColor : borderColor

    const iconScale = this.state.scaleCheckmarkValue.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [0.01, 1.6, 1],
    })

    const scaleValue = selected ? 1 : 0

    this.scaleCheckmark(scaleValue)

    return (
      <Box
        width={30}
        height={30}
        borderRadius={15}
        borderWidth={1}
        borderColor={border}
        center
        middle
        backgroundColor={background}
      >
        <Animated.View style={[{ transform: [{ scale: iconScale }] }]}>
          <Icon type="ionicon" name="md-checkmark" color={iconColor} size={20} />
        </Animated.View>
      </Box>
    )
  }
}
