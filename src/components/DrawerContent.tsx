import * as React from 'react'
import { DrawerItems, DrawerItemsProps } from 'react-navigation'

import { Me } from '../graphql/types'
import { signOut } from '../services/Auth'

import { Box, Flex } from '../system/components'

import { Avatar } from './Avatar'
import { TextButton } from './TextButton'

interface DrawerContentProps extends DrawerItemsProps {
  user: Me
}

export const DrawerContent = (props: DrawerContentProps) => (
  <Flex>
    <Box flex={8}>
      <Box flex={4} mt={30} center middle>
        <Avatar size={150} source={{ uri: props.user.profileImage }} />
      </Box>
      <Box flex={6}>
        <DrawerItems {...props} />
      </Box>
    </Box>
    <Box flex={2} center middle>
      <TextButton onPress={() => signOut()}>Logout</TextButton>
    </Box>
  </Flex>
)
