import { LinearGradient } from 'expo'
import * as React from 'react'
import { withTheme } from 'styled-components/native'

import { Box, Color, GradientColor, Row, Text, Touchable, WithTheme } from '../system'

import { Icon, IconProps } from './Icon'
import { Spinner } from './Spinner'

interface SizeParams {
  height: number
  borderRadius: number
}

type ButtonType = 'default' | 'bordered' | 'gradient'
type ButtonSize = 'large' | 'medium' | 'small'
type ButtonSizes = Record<ButtonSize, SizeParams>

const ButtonSizes: ButtonSizes = {
  large: { height: 60, borderRadius: 30 },
  medium: { height: 44, borderRadius: 22 },
  small: { height: 24, borderRadius: 12 },
}

interface BigButtonProps extends Touchable.HighlightProps {
  title?: string
  iconProps?: IconProps
  size?: ButtonSize
  loading?: boolean
  disabled?: boolean
  pushable?: boolean
  type?: ButtonType
  gradientColor?: GradientColor
  iconColor?: Color
  color?: Color
  textColor?: Color
  children?: React.ReactNode
  onPress?: () => any | void
}

export const BigRoundedButton = withTheme((props: WithTheme<BigButtonProps>) => {
  const {
    title,
    textColor = 'shade9',
    iconProps,
    iconColor,
    size = 'medium',
    loading,
    disabled,
    color = 'accent1',
    type,
    gradientColor = 'default',
    style,
    onPress,
    children,
    theme,
    pushable,
    ...rest
  } = props

  const bg: Color = color

  const sizeStyle = ButtonSizes[size]

  const innerContent = (
    <Row center middle height={sizeStyle.height}>
      {iconProps && <Icon {...iconProps} />}
      {loading && <Spinner color={textColor} />}
      {!loading && title && <Text color={type === 'bordered' ? bg : textColor}>{title}</Text>}
      {children}
    </Row>
  )

  const { height, borderRadius } = ButtonSizes[size]

  const gradientArray = theme.gradients[gradientColor] || theme.gradients.default

  return (
    <Touchable.Highlight
      height={height}
      borderRadius={borderRadius}
      onPress={onPress}
      disabled={disabled}
      style={style}
      {...rest}
    >
      <Box height={sizeStyle.height} borderRadius={sizeStyle.borderRadius}>
        {type === 'gradient' ? (
          <LinearGradient start={[1, 0]} end={[1, 1]} colors={gradientArray}>
            {innerContent}
          </LinearGradient>
        ) : (
          innerContent
        )}
      </Box>
    </Touchable.Highlight>
  )
})
