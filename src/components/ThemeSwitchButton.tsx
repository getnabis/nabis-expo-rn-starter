import * as React from 'react'

import { withThemeToggle } from '../hoc/withToggleTheme'
import { TextButton } from './TextButton'

export const ThemeSwitchButton = withThemeToggle(({ toggle }) => <TextButton onPress={toggle} title="Toggle Theme" />)
