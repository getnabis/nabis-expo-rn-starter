import { storiesOf } from '@storybook/react-native'
import * as React from 'react'
import { FormField } from '../TextInput/FormField'

storiesOf('FormField', module)
  .add('default', () => <FormField name="name" label="Name" />)
  .add('error', () => <FormField name="name" label="Name" error="Name is a required field" />)
  .add('secure', () => <FormField name="password" label="Password" secureTextEntry />)
