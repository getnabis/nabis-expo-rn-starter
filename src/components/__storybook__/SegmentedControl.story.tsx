import { storiesOf } from '@storybook/react-native'
import * as React from 'react'
import { SegmentedControl } from '../SegmentedControl'

storiesOf('SegmentedControl', module).add('default', () => (
  <SegmentedControl
    values={['item 1', 'item 2', 'item 3']}
    selectedIndex={1}
    selectedColor="accent2"
    onChange={() => {}}
  />
))
