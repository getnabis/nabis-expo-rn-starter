import { storiesOf } from '@storybook/react-native'
import * as React from 'react'
import { GoogleSignInButton } from '../GoogleSignInButton'

storiesOf('GoogleSignInButton', module).add('default', () => <GoogleSignInButton />)
