import { storiesOf } from '@storybook/react-native'
import * as React from 'react'

import { TabBarIcon } from '../TabBarIcon'

storiesOf('TabBarIcon', module).add('default', () => (
  <TabBarIcon name="phone" tintColor="accent1" focused={false} horizontal={false} />
))
