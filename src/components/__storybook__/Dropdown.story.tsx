import { storiesOf } from '@storybook/react-native'
import * as React from 'react'
import { Dropdown } from '../Dropdown'

import { randomData } from '../../utils/randomData'

storiesOf('Dropdown', module).add('default', () => <Dropdown data={randomData()} />)
