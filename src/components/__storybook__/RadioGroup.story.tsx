import { storiesOf } from '@storybook/react-native'
import * as React from 'react'
import { RadioGroup } from '../RadioGroup'

storiesOf('RadioGroup', module).add('default', () => (
  <RadioGroup
    onChange={(index: number) => {
      console.log(`you selected index: ${index}`)
    }}
    items={['option 1', 'option 2']}
  />
))
