import * as React from 'react'
import { Platform, TouchableNativeFeedback } from 'react-native'

import { Box, Color, FontWeight, Text, Touchable } from '../system'
import { Spinner } from './Spinner'

interface RoundedButtonProps {
  text?: string
  textColor?: Color
  textSize?: number
  textWeight?: FontWeight
  textAlign?: 'left' | 'center' | 'right'
  background?: Color
  borderColor?: Color
  icon?: object
  iconPosition?: string
  handleOnPress?: () => any
  disabled?: boolean
  loading?: boolean
}

export default class RoundedButton extends React.Component<RoundedButtonProps> {
  render() {
    const {
      loading,
      disabled,
      text,
      textColor,
      background,
      icon,
      handleOnPress,
      textSize = 16,
      textWeight = 'bold',
      iconPosition = 'left',
      textAlign = 'center',
      borderColor = 'white',
    } = this.props

    const backgroundColor = background || 'transparent'
    const color: Color = textColor || 'shade0'
    const opacityStyle = disabled || loading ? 0.5 : 1
    const textOpacity = loading ? 0 : 1
    const rippleColor = backgroundColor === 'transparent' ? color : 'rgba(0,0,0,0.4)'

    const ButtonComponent = (buttonProps: any) => {
      if (Platform.OS === 'ios') {
        return (
          <Touchable.Highlight
            opacity={opacityStyle}
            backgroundColor={backgroundColor}
            borderColor={borderColor}
            mb={2}
            borderRadius={40}
            borderWidth={1}
            onPress={handleOnPress}
            activeOpacity={0.6}
            disabled={disabled || loading}
          >
            {buttonProps.children}
          </Touchable.Highlight>
        )
      }
      return (
        <Box overflow="hidden" borderRadius={40} borderWidth={1} mb={2} borderColor={borderColor}>
          <Touchable.NativeFeedback
            useForeground={true}
            onPress={handleOnPress}
            disabled={disabled || loading}
            background={TouchableNativeFeedback.Ripple(rippleColor, false)}
          >
            {buttonProps.children}
          </Touchable.NativeFeedback>
        </Box>
      )
    }

    const left = iconPosition === 'left' && !loading && icon
    const right = iconPosition === 'right' && !loading && icon

    return (
      <ButtonComponent>
        <Box opacity={opacityStyle} backgroundColor={backgroundColor} px={4} py={2} center middle>
          {left}
          {loading && (
            <Box
              width={90}
              height={90}
              borderRadius={15}
              position="absolute"
              left="50%"
              top="50%"
              marginLeft={-45}
              marginTop={-45}
            >
              <Spinner />
            </Box>
          )}
          <Text color={color} size={textSize} textAlign={textAlign} fontWeight={textWeight} opacity={textOpacity}>
            {text}
          </Text>
          {right}
        </Box>
      </ButtonComponent>
    )
  }
}
