import * as React from 'react'

import { Color, Text, Touchable } from '../system'

interface TextButtonProps extends Touchable.OpacityProps {
  title?: string
  activeTint?: Color
  inactiveTint?: Color
  disabled?: boolean
}

export const TextButton: React.FC<TextButtonProps> = ({
  title,
  disabled,
  children,
  activeTint = 'accent1',
  inactiveTint = 'shade7',
  ...props
}) => (
  <Touchable.Opacity {...props}>
    <Text color={disabled ? inactiveTint : activeTint}>{children || title}</Text>
  </Touchable.Opacity>
)
