import * as React from 'react'
import { Box, Col, H5, Image, Row, Small } from '../system/components'
import { Card } from './Card'
import { Icon } from './Icon'

interface Props {
  image?: string
  avatar?: string
  location?: string
  title?: string
  caption?: string
  timestamp?: string
  onPress?: () => void
}

const CardImage: React.FC<Props> = ({ image }) => (
  <Box overflow="hidden">
    <Image source={{ uri: image }} height={200} />
  </Box>
)

const CardAvatar: React.FC<Props> = ({ avatar }) =>
  avatar && <Image source={{ uri: avatar }} height={40} width={40} borderRadius={20} />

const CardLocation: React.FC<Props> = ({ location }) => (
  <Row>
    <Icon type="feather" size={2} name="map-pin" color="shade6" />
    <Small muted ml={1}>
      {location}
    </Small>
  </Row>
)

const CardFooter: React.FC<Props> = ({ avatar, title, caption, location }) => (
  <Row>
    <Col flex={0.3}>{avatar && <CardAvatar avatar={avatar} />}</Col>
    <Col flex={1.7}>
      <Box middle>
        <H5>{title}</H5>
      </Box>
      <Row space="between">
        <Small muted>{caption}</Small>
        {location && <CardLocation location={location} />}
      </Row>
    </Col>
  </Row>
)

export const CardWithContent: React.FC<Props> = props => (
  <Card>
    <CardImage {...props} />
    <CardFooter {...props} />
  </Card>
)
