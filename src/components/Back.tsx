import * as React from 'react'

import { Box, Color, Touchable } from '../system'
import { Icon } from './Icon'

interface BackProps extends Touchable.OpacityProps {
  tintColor?: Color
}

export const Back: React.FC<BackProps> = ({ tintColor = 'shade9', ...props }) => (
  <Touchable.Opacity {...props}>
    <Box width={60} height={48}>
      <Icon type="feather" color={tintColor} name="chevron-left" size={48} />
    </Box>
  </Touchable.Opacity>
)
