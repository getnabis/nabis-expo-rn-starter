import * as React from 'react'

import { Box, BoxProps, SafeAreaView } from '../system'
import { Icon, IconProps } from './Icon'
import { TextField, TextFieldProps } from './TextInput/TextField'

interface SearchBarProps extends TextFieldProps {
  containerProps: BoxProps
  searchBoxProps: BoxProps
  iconProps: IconProps
}

export const SearchBar: React.FC<SearchBarProps> = ({
  containerProps,
  searchBoxProps,
  iconProps,
  ...textInputProps
}) => (
  <Box position="absolute" top={0} left={0} right={0} bg="shade9" {...containerProps}>
    <SafeAreaView>
      <Box
        borderWidth={0}
        borderColor="shade1"
        bg="shade0"
        borderRadius="medium"
        mx={2}
        flex={1}
        height={40}
        flexDirection="row"
        {...searchBoxProps}
      >
        <Box center middle mr={1}>
          <Icon type="ionicon" name="ios-search" {...iconProps} />
        </Box>
        <TextField {...textInputProps} />
      </Box>
    </SafeAreaView>
  </Box>
)
