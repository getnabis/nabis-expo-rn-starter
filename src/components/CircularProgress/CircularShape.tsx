import * as React from 'react'

// import { LineCapType } from 'expo'
import { Animated, View, ViewProps, ViewStyle } from 'react-native'

import { G, Path, Svg } from '../../omega/react-native-svg'

/* CIRCULAR SHAPE */

export interface CircularShapeProps extends ViewProps {
  size?: number
  fill?: number
  width?: number
  backgroundWidth?: number
  backgroundColor?: string
  tintColor?: string
  rotation?: number
  // lineCap?: LineCapType
  arcSweepAngle?: number
}

const polarToCartesian = (centerX: number, centerY: number, radius: number, angleInDegrees: number) => {
  const angleInRadians = ((angleInDegrees - 90) * Math.PI) / 180.0
  return {
    x: centerX + radius * Math.cos(angleInRadians),
    y: centerY + radius * Math.sin(angleInRadians),
  }
}

const circlePath = (x: number, y: number, radius: number, startAngle: number, endAngle: number) => {
  const start = polarToCartesian(x, y, radius, endAngle * 0.9999)
  const end = polarToCartesian(x, y, radius, startAngle)
  const largeArcFlag = endAngle - startAngle <= 180 ? '0' : '1'
  const d = ['M', start.x, start.y, 'A', radius, radius, 0, largeArcFlag, 0, end.x, end.y]
  return d.join(' ')
}

const clampFill = (fill: number) => Math.min(100, Math.max(0, fill))

class CircularShape extends React.Component<CircularShapeProps> {
  render() {
    const {
      size,
      fill,
      width,
      backgroundWidth,
      backgroundColor,
      tintColor,
      rotation = 90,
      // lineCap = 'butt',
      arcSweepAngle = 360,
      style,
      children,
    } = this.props
    const maxWidthCircle = backgroundWidth ? Math.max(width, backgroundWidth) : width
    const backgroundPath = circlePath(size / 2, size / 2, size / 2 - maxWidthCircle / 2, 0, arcSweepAngle)
    const path = circlePath(
      size / 2,
      size / 2,
      size / 2 - maxWidthCircle / 2,
      0,
      (arcSweepAngle * clampFill(fill)) / 100,
    )
    const offset = size - maxWidthCircle * 2

    const childContainerStyle: ViewStyle = {
      position: 'absolute',
      left: maxWidthCircle,
      top: maxWidthCircle,
      width: offset,
      height: offset,
      borderRadius: offset / 2,
      alignItems: 'center',
      justifyContent: 'center',
      overflow: 'hidden',
    }

    return (
      <View style={style}>
        <Svg width={size} height={size} style={{ backgroundColor: 'transparent' }}>
          <G rotation={rotation} originX={size / 2} originY={size / 2}>
            {backgroundColor && (
              <Path
                d={backgroundPath}
                stroke={backgroundColor}
                strokeWidth={backgroundWidth || width}
                // strokeLinecap={lineCap}
                fill="transparent"
              />
            )}
            {fill > 0 && (
              <Path d={path} stroke={tintColor} strokeWidth={width} strokeLinecap={lineCap} fill="transparent" />
            )}
          </G>
        </Svg>
        {children && <View style={childContainerStyle}>{children}</View>}
      </View>
    )
  }
}

/* COMPONENT */

export const CircularShapeAnimated = Animated.createAnimatedComponent(CircularShape)
