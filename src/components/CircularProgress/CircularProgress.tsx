import * as React from 'react'

import { Animated, Easing, EasingFunction } from 'react-native'

import { CircularShapeAnimated, CircularShapeProps } from './CircularShape'

interface Props extends CircularShapeProps {
  duration?: number
  easing?: (value: number) => number
  prefill?: number
  onAnimationComplete?: () => {}
}

interface State {
  fillAnimation: Animated.Value
}

export class CircularProgress extends React.PureComponent<Props, State> {
  static defaultProps: Props = {
    duration: 500,
    easing: Easing.out(Easing.ease),
    prefill: 0,
  }

  constructor(props: Props) {
    super(props)
    this.state = {
      fillAnimation: new Animated.Value(props.prefill),
    }
  }

  componentDidMount() {
    this.animate()
  }

  componentDidUpdate(prevProps: Props) {
    if (prevProps.fill !== this.props.fill) {
      this.animate()
    }
  }

  reAnimate = (prefill: number, toVal: number, dur: number, ease: EasingFunction) => {
    this.setState(
      {
        fillAnimation: new Animated.Value(prefill),
      },
      () => this.animate(toVal, dur, ease),
    )
  }

  animate = (toVal?: number, dur?: number, ease?: EasingFunction) => {
    const toValue = toVal && toVal >= 0 ? toVal : this.props.fill
    const duration = dur || this.props.duration
    const easing = ease || this.props.easing

    const anim = Animated.timing(this.state.fillAnimation, {
      toValue,
      easing,
      duration,
    })
    anim.start(this.props.onAnimationComplete)

    return anim
  }

  render() {
    return <CircularShapeAnimated {...this.props} fill={this.state.fillAnimation} />
  }
}
