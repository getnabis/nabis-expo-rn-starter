import * as React from 'react'
import { Animated, Easing } from 'react-native'

import { styled, Text } from '../system'
import { Icon } from './Icon'

interface Props {
  showNotification: boolean
  type: string
  firstLine?: string
  secondLine?: string
  handleCloseNotification: () => any
}

interface State {
  positionValue: Animated.Value
}

export class Notification extends React.Component<Props, State> {
  state = {
    positionValue: new Animated.Value(-60),
  }

  animateNotification = (value: number) => {
    Animated.timing(this.state.positionValue, {
      toValue: value,
      duration: 300,
      easing: Easing.back(2),
    }).start()
  }

  closeNotification = () => {
    this.props.handleCloseNotification()
  }

  render() {
    const { type, firstLine, secondLine, showNotification } = this.props
    showNotification ? this.animateNotification(0) : this.animateNotification(-60)
    const { positionValue } = this.state
    return (
      <AnimatedWrapper style={{ marginBottom: positionValue }}>
        <ContentContainer>
          <ContentMessage>
            <ContentText color={type === 'error' ? 'error' : undefined}>{type}</ContentText>
            <Text>{firstLine}</Text>
          </ContentMessage>
          <ContentMessage>{secondLine}</ContentMessage>
        </ContentContainer>
        <CloseButton onPress={this.closeNotification}>
          <Icon name="times" size="small" color="accent2" />
        </CloseButton>
      </AnimatedWrapper>
    )
  }
}

const AnimatedWrapper = styled(Animated.View)(({ theme }) => ({
  flex: 1,
  backgroundColor: theme.colors.white,
  height: 60,
  padding: 10,
}))

const ContentContainer = styled.View({
  flex: 1,
  flexDirection: 'column',
  flexWrap: 'wrap',
  alignItems: 'flex-start',
})

const ContentText = styled(Text)({
  marginRight: 5,
  fontSize: 14,
  marginBottom: 2,
})

const ContentMessage = styled.View({
  flexDirection: 'row',
  flex: 1,
  marginBottom: 2,
  fontSize: 14,
})

const CloseButton = styled.TouchableOpacity({
  position: 'absolute',
  right: 10,
  top: 10,
  zIndex: 999,
})
