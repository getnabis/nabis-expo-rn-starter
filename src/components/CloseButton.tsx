import { IconButton } from './IconButton'

export const CloseButton = IconButton

CloseButton.displayName = 'CloseButton'
CloseButton.defaultProps = {
  iconProps: {
    size: 24,
    name: 'close',
    type: 'feather',
  },
}
