import { withTheme } from 'styled-components/native'
import { TextInput, TextInputProps, WithTheme } from '../../system'

export interface TextFieldProps extends TextInputProps {
  name: string
  error?: string | boolean
}

export const TextField = withTheme(({ theme, name, error, ...props }: WithTheme<TextFieldProps>) => (
  <TextInput
    placeholder={name}
    placeholderTextColor={error ? 'error' : 'shade6'}
    color={error ? 'error' : 'shade9'}
    {...props}
  />
))

TextField.displayName = 'TextField'
TextField.defaultProps = {
  fontSize: 3,
  backgroundColor: 'shade9',
  borderColor: 'shade8',
  borderWidth: 1,
  height: 30,
  autoCapitalize: 'none',
}
