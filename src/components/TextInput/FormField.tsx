import * as React from 'react'
import { TextStyle, ViewStyle } from 'react-native'

import { Box, Flex, Small, Touchable } from '../../system'

import { useState } from '../../hooks'
import { Icon } from '../Icon'
import { TextField, TextFieldProps } from './TextField'

interface FormFieldProps extends TextFieldProps {
  // name: string
  // placeholder?: string
  // autoFocus?: boolean
  // color?: Color
  // iconColor?: Color
  // style?: ViewStyle
  // error?: string | boolean
  containerStyle?: ViewStyle
  labelStyle?: TextStyle
  label?: string
  type?: 'text' | 'email' | 'password'
  success?: boolean
}

export const FormField = ({
  autoCapitalize,
  autoFocus,
  containerStyle,
  defaultValue,
  error,
  fontSize,
  label,
  labelStyle,
  name,
  onChangeText,
  placeholder,
  secureTextEntry,
  style,
  success,
  type,
  color,
  placeholderTextColor = 'shade6',
  ...props
}: FormFieldProps) => {
  const [secureInput, setSecureInput] = useState(secureTextEntry || (type !== 'text' && type !== 'email'))
  const [inputValue, setInputValue] = useState(defaultValue)

  const toggleSecure = () => {
    setSecureInput(!secureInput)
  }

  const handleChangeText = (text: string) => {
    setInputValue(text)
    onChangeText(text)
  }

  return (
    <Flex style={containerStyle} pb={1}>
      <Small fontWeight="bold" color="shade2" mb={2} style={labelStyle}>
        {label}
      </Small>
      <TextField
        name={name}
        secureTextEntry={secureInput}
        onChangeText={handleChangeText}
        keyboardType={type === 'email' ? 'email-address' : 'default'}
        autoCorrect={false}
        underlineColorAndroid="transparent"
        defaultValue={inputValue}
        value={inputValue}
        placeholder={placeholder}
        autoFocus={autoFocus}
        autoCapitalize={autoCapitalize ? 'sentences' : undefined}
        placeholderTextColor={error ? 'error' : placeholderTextColor}
        color={error ? 'error' : 'shade9'}
        {...props}
      />

      {secureTextEntry && !error && !success && (
        <Touchable.PressableView onPress={toggleSecure} position="absolutee" right={0} bottom={12}>
          <Box position="absolute" right={0} bottom={12}>
            <Icon type="feather" name="eye" color={color} size={fontSize} />
          </Box>
        </Touchable.PressableView>
      )}

      {success && (
        <Box position="absolute" right={0} bottom={12}>
          <Icon type="feather" name="check" color="success" size={fontSize} />
        </Box>
      )}

      {error && (
        <Box position="absolute" right={0} bottom={12}>
          <Icon type="feather" name="x-circle" color="error" size={fontSize} />
        </Box>
      )}

      <Small color={error ? 'error' : 'transparent'}>error</Small>
    </Flex>
  )
}

FormField.defaultProps = {
  label: '',
  type: 'text',
  style: {},
  containerStyle: {},
  labelStyle: {},
  success: false,
  error: false,
  placeholderColor: 'shade3',
  textColor: 'shade0',
  iconColor: 'shade0',
}
