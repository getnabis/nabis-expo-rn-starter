import * as React from 'react'
import { TextField, TextFieldProps } from 'react-native-material-textfield'
import { combined, CombinedProps, styled, withTheme, WithTheme } from '../../system'

const StyledMaterialInput = styled(TextField)<CombinedProps>(combined)

export interface MaterialTextInputProps extends CombinedProps, TextFieldProps {
  fontSize: number
}

export const MaterialTextInput = withTheme(({ theme, ...props }: WithTheme<MaterialTextInputProps>) => {
  return (
    <StyledMaterialInput
      labelHeight={12}
      baseColor={theme.colors.shade5}
      tintColor={theme.colors.accent1}
      textColor={theme.colors.shade9}
      errorColor={theme.colors.error}
      {...props}
    />
  )
})
