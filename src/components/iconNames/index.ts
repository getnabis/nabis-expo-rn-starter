import { awesome5 } from './awesome5'
import { feather } from './feather'
import { ionicon } from './ionicon'
import { material } from './material'

export const iconNames = {
  awesome5,
  feather,
  ionicon,
  material,
}

export type IconType = keyof typeof iconNames

export type IconNameAccessor = (t: typeof iconNames) => string | string

export const getIconName = (accessor: IconNameAccessor) =>
  typeof accessor === 'string' ? accessor : accessor(iconNames)

export const testGetIconName = getIconName((t) => t.awesome5.ribbon)
