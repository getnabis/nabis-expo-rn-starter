import * as React from 'react'
import { Modal, ModalProps } from 'react-native'

import { Box } from '../system'
import { Spinner } from './Spinner'

export const LoadingModal: React.FC<ModalProps> = props => (
  <Modal {...props}>
    <Box center middle bg="black80" flex={1}>
      <Spinner />
    </Box>
  </Modal>
)
