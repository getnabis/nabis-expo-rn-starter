import React from 'react'
import {
  ActivityIndicator,
  Animated,
  StyleProp,
  StyleSheet,
  TouchableWithoutFeedback,
  View,
  ViewStyle,
} from 'react-native'

interface Props {
  width: number
  height: number
  title?: string
  titleColor?: string
  titleFontFamily?: string
  titleFontSize?: number
  backgroundColor?: string
  borderWidth?: number
  borderRadius?: number
  activityIndicatorColor?: string
  onPress: () => void
  customStyles?: StyleProp<ViewStyle>
}

interface State {
  showLoading: boolean
}

export default class Component extends React.PureComponent<Props, State> {
  static defaultProps = {
    title: 'Button',
    titleColor: 'white',
    backgroundColor: 'gray',
    activityIndicatorColor: 'white',
    borderRadius: 0,
    customStyles: {},
  }

  loadingValue: {
    width: Animated.Value
    borderRadius: Animated.Value
    opacity: Animated.Value
  }

  constructor(props: Props) {
    super(props)

    this.state = {
      showLoading: false,
    }

    this.loadingValue = {
      width: new Animated.Value(props.width),
      borderRadius: new Animated.Value(props.borderRadius),
      opacity: new Animated.Value(1),
    }
  }

  showLoading = (showLoading: boolean) => {
    if (showLoading) {
      this._loadingAnimation(this.props.width, this.props.height, this.props.borderRadius, this.props.height / 2, 1, 0)
      this.setState({ showLoading })
    } else {
      setTimeout(() => {
        this._loadingAnimation(
          this.props.height,
          this.props.width,
          this.props.height / 2,
          this.props.borderRadius,
          0,
          1,
        )
        this.setState({ showLoading })
      }, 1000)
    }
  }

  _loadingAnimation = (
    widthStart: any,
    widthEnd: any,
    borderRadiusStart: any,
    borderRadiusEnd: any,
    opacityStart: any,
    opacityEnd: any,
  ) => {
    if (this.loadingValue.width !== widthEnd) {
      this.loadingValue.width.setValue(widthStart)
      this.loadingValue.opacity.setValue(opacityStart)
      this.loadingValue.borderRadius.setValue(borderRadiusStart)

      Animated.timing(this.loadingValue.width, {
        toValue: widthEnd,
        duration: 400,
      }).start()

      Animated.timing(this.loadingValue.borderRadius, {
        toValue: borderRadiusEnd,
        duration: 400,
      }).start()

      Animated.timing(this.loadingValue.opacity, {
        toValue: opacityEnd,
        duration: 300,
      }).start()
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <TouchableWithoutFeedback onPress={!this.state.showLoading ? this.props.onPress : null}>
          <Animated.View
            style={[
              styles.containerButton,
              {
                width: this.loadingValue.width,
                height: this.props.height,
                backgroundColor: this.props.backgroundColor,
                borderWidth: this.props.borderWidth,
                borderRadius: this.loadingValue.borderRadius,
              },
            ]}
          >
            {this.state.showLoading ? this._renderIndicator() : this._renderTitle()}
          </Animated.View>
        </TouchableWithoutFeedback>
      </View>
    )
  }

  _renderTitle() {
    return (
      <Animated.Text
        style={[
          styles.buttonText,
          {
            opacity: this.loadingValue.opacity,
            color: this.props.titleColor,
            fontFamily: this.props.titleFontFamily,
            fontSize: this.props.titleFontSize,
          },
          this.props.customStyles,
        ]}
      >
        {this.props.title}
      </Animated.Text>
    )
  }

  _renderIndicator() {
    return <ActivityIndicator color={this.props.activityIndicatorColor} />
  }
}

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
  },
  containerButton: {
    justifyContent: 'center',
  },
  buttonText: {
    backgroundColor: 'transparent',
    textAlign: 'center',
  },
})
