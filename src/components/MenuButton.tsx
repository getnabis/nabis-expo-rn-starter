import * as React from 'react'

import { openDrawer } from '../services/Navigation'
import { HeaderButton, HeaderButtonItem, HeaderButtons, HeaderButtonsProps } from './HeaderButtons'

export const MenuButton = (props: HeaderButtonsProps) => (
  <HeaderButtons HeaderButtonComponent={HeaderButton} {...props}>
    <HeaderButtonItem title="menu" iconName="menu" onPress={openDrawer} />
  </HeaderButtons>
)
