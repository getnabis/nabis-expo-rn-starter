import * as React from 'react'

import { Touchable } from '../system'
import { Icon, IconProps } from './Icon'

export interface IconButtonProps extends Touchable.OpacityProps {
  iconProps: IconProps
}

export const IconButton: React.FC<IconButtonProps> = ({ iconProps, ...props }) => (
  <Touchable.Opacity {...props}>
    <Icon {...iconProps} />
  </Touchable.Opacity>
)
