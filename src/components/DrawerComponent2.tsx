import * as React from 'react'
import { DrawerItems, DrawerItemsProps, SafeAreaView, ScrollView } from 'react-navigation'

import { Text } from '../system'
import { BigRoundedButton } from './BigRoundedButton'

import { navigateTo } from '../services/Navigation'

interface DrawerComponentProps extends DrawerItemsProps {}

export const DrawerComponent2: React.FC<DrawerComponentProps> = props => (
  <ScrollView>
    <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always', horizontal: 'never' }}>
      <Text>This is the drawer</Text>
      <DrawerItems {...props} />
      <BigRoundedButton title="Settings" onPress={() => navigateTo('Settings')} />
    </SafeAreaView>
  </ScrollView>
)
