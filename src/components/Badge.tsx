import * as React from 'react'
import { Box, BoxProps, Tiny } from '../system/components'

export const Badge: React.FC<BoxProps> = ({ children, ...props }) => (
  <Box {...props}>
    <Tiny color="white">{children}</Tiny>
  </Box>
)

Badge.displayName = 'Badge'
Badge.defaultProps = {
  color: 'accent2',
  borderRadius: 2,
  px: 1,
  py: 1,
}
