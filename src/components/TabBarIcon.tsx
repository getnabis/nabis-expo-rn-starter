import * as React from 'react'
import { TabBarIconProps as RNTabBarIconProps } from 'react-navigation'

import { Color } from '../system'
import { Icon, IconProps, IconType } from './Icon'

interface TabBarIconProps extends RNTabBarIconProps, IconProps {
  tintColor: Color
  focused: boolean
}

export const TabBarIcon = ({ tintColor, focused, ...props }: TabBarIconProps) => (
  <Icon color={focused ? tintColor : 'shade9'} {...props} />
)

export const makeTabBarIcon = (name: string, type: IconType = 'feather', size: number = 24) => ({
  tintColor,
  focused,
  horizontal,
}: RNTabBarIconProps) => (
  <TabBarIcon
    horizontal={horizontal}
    name={name}
    type={type}
    size={size}
    tintColor={tintColor as Color}
    focused={focused}
  />
)
