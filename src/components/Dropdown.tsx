import { Dropdown, DropDownProps } from 'react-native-material-dropdown'

export interface DropdownProps extends DropDownProps {
  flex?: boolean
}

export { Dropdown }
