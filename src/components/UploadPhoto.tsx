import * as React from 'react'
import { Dimensions } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'

import { goBack } from '../services/Navigation'
import { Box, Image, ScrollView, Touchable } from '../system'
import { IconButton } from './IconButton'

interface Props extends NavigationScreenProp {}
interface State {
  selectedImageIndex?: number | null
}

export class UploadPhoto extends React.Component<Props, State> {
  static navigationOptions = {
    title: 'Upload Photo',
  }

  componentDidMount() {
    const { selectedImageIndex } = this.props.navigation.state.params.data

    if (selectedImageIndex) {
      this.setState(() => ({ selectedImageIndex }))
    }
  }

  updateSelectedImage = (image: any, index: any) => {
    const { updateSelectedImage } = this.props.navigation.state.params
    updateSelectedImage(image, index)
    if (index === this.state.selectedImageIndex) {
      index = null
    }
    this.setState(() => ({ selectedImageIndex: index }))
  }

  render() {
    const { selectedImageIndex } = this.state
    const { images, selectedImage } = this.props.navigation.state.params.data

    console.log({ selectedImage, message: 'not sure what to do with this' })

    const { width: windowWidth, height: windowHeight } = Dimensions.get('window')

    return (
      <Box flex={1}>
        <Box height={windowHeight - 60} width={windowWidth}>
          <ScrollView flexDirection="row" flexWrap="wrap">
            {images.map((image: any, index: any) => {
              return (
                <Touchable.WithoutFeedback key={index} onPress={() => this.updateSelectedImage(image, index)}>
                  <Image
                    source={{ uri: image.node.image.uri }}
                    style={{
                      width: windowWidth / 2,
                      height: windowWidth / 2,
                      opacity: selectedImageIndex === index ? 0.7 : 1,
                    }}
                  />
                </Touchable.WithoutFeedback>
              )
            })}
          </ScrollView>
        </Box>
        {selectedImageIndex ||
          (selectedImageIndex === 0 && (
            <Box position="absolute" bottom={5} right={25} size={50}>
              <IconButton
                onPress={() => goBack()}
                iconProps={{
                  type: 'feather',
                  name: 'check',
                  size: 34,
                }}
                size={50}
              />
            </Box>
          ))}
      </Box>
    )
  }
}
