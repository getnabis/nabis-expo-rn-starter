import * as React from 'react'

import { Box, BoxProps } from '../system'
import { Spinner } from './Spinner'

export const LoadingView = (props: BoxProps) => (
  <Box flex={1} {...props}>
    <Box center middle flex={1}>
      <Spinner color="shade9" />
    </Box>
  </Box>
)
