import * as React from 'react'
import styled from 'styled-components/native'
import { Color, getColor, Tiny } from '../system'
import { Icon, IconType } from './Icon'

interface IconWithBadgeProps {
  name: string
  badgeCount?: number
  color: Color
  size: number
  type?: IconType
}

export const IconWithBadge: React.FC<IconWithBadgeProps> = ({ type, name, badgeCount = 0, color, size }) => (
  <Wrapper>
    <Icon type={type} name={name} size={size} color={color} />
    {badgeCount > 0 && (
      <Badge>
        <Tiny color="white" fontWeight="bold">
          {badgeCount}
        </Tiny>
      </Badge>
    )}
  </Wrapper>
)

const Wrapper = styled.View({
  width: 24,
  height: 24,
  margin: 5,
})

const Badge = styled.View(props => ({
  position: 'absolute',
  right: -6,
  top: -3,
  backgroundColor: getColor('accent3')(props),
  borderRadius: 6,
  width: 12,
  height: 12,
  justifyContent: 'center',
  alignItems: 'center',
}))
