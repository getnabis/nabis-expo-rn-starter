import * as React from 'react'
import { Animated, TouchableWithoutFeedback } from 'react-native'
import { StateHandlerMap, withStateHandlers } from 'recompose'
import { space } from 'styled-system'

import { Color, styled, ThemedProps } from '../system'
import { Spinner } from './Spinner'

interface Outters {
  color?: Color
  onPress?: () => void
  loading?: boolean
  iconComponent?: React.ReactNode
  title?: string
  children?: React.ReactNode
  spinnerColor?: Color

  small?: boolean
  large?: boolean
  primary?: boolean
  secondary?: boolean
}

interface State extends Outters {
  scale: Animated.Value
  isPressing: boolean
}

interface Updaters extends StateHandlerMap<State> {
  onPressIn: () => State
  onPressOut: () => State
}

const RoundedButton = withStateHandlers<State, Updaters, Outters>(
  ({ color = 'white', spinnerColor = 'white', ...rest }) => ({
    scale: new Animated.Value(1),
    isPressing: false,
    color,
    spinnerColor,
    ...rest,
  }),
  {
    onPressIn: (state: State) => () => {
      Animated.spring(state.scale, {
        toValue: 0.95,
      }).start()

      return { ...state, isPressing: true }
    },

    onPressOut: (state: State) => () => {
      Animated.spring(state.scale, {
        toValue: 1,
        friction: 3,
        tension: 40,
      }).start()

      return { ...state, isPressing: false }
    },
  },
)(
  ({
    onPress,
    loading,
    iconComponent,
    color,
    title,
    scale,
    onPressIn,
    onPressOut,
    children,
    spinnerColor = 'white',
  }) => (
    <Container>
      <TouchableWithoutFeedback onPress={onPress} onPressIn={onPressIn} onPressOut={onPressOut}>
        <ShadowWrapper>
          <Button color={color} style={{ transform: [{ scale }] }}>
            {loading && <Spinner color={spinnerColor} />}
            {!loading && iconComponent}
            {!loading && (title ? <Caption>{title}</Caption> : children)}
          </Button>
        </ShadowWrapper>
      </TouchableWithoutFeedback>
    </Container>
  ),
)

const ButtonStyles = {
  small: {
    containerStyle: {
      minHeight: 20,
      borderRadius: 10,
    },
    textStyle: {},
  },
  regular: {
    containerStyle: {
      minHeight: 30,
      borderRadius: 15,
    },
    textStyle: {},
  },
  large: {
    containerStyle: {
      minHeight: 52,
      borderRadius: 12,
    },
    textStyle: {},
  },
}

export type ButtonSizeType = keyof typeof ButtonStyles

const ShadowWrapper = styled.View({
  shadowOffset: { x: 4, y: 4 },
  shadowSize: 4,
  shadowColor: '#000000',
  elevation: 2,
})

const Container = styled.View(({ theme }) => ({
  backgroundColor: theme.colors.accent1,
}))

const Caption = styled.Text({
  fontSize: 13,
  color: 'white',
})

const Button = styled(Animated.View)(
  ({ theme }: ThemedProps) => ({
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: theme.colors.accent1,
    minHeight: 30,
    borderRadius: 15,
  }),
  space,
)

export default RoundedButton
