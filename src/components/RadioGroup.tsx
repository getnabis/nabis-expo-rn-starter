import * as React from 'react'

import { Box, BoxProps, Color, getColor, styled, Text } from '../system'

interface StateProps {
  underline?: boolean
  active?: boolean
  activeColor?: Color
  inActiveColor?: Color
}

interface RadioGroupProps extends BoxProps, StateProps {
  items: any[]
  selectedIndex?: number
  onChange: (index: number) => void
}

const Underline = styled(Box)<StateProps>(({ active, activeColor, inActiveColor, ...props }) => ({
  backgroundColor: getColor(active ? activeColor : inActiveColor)(props),
}))

Underline.displayName = 'Underline'
Underline.defaultProps = {
  height: 3,
  bottom: 0,
  left: 0,
  right: 0,
  mt: 3,
  activeColor: 'accent1',
  inActiveColor: 'shade7',
  position: 'absolute',
}

export const RadioGroup: React.FC<RadioGroupProps> = ({ items, selectedIndex, onChange, underline, activeColor }) => (
  <Box borderColor="accent1" borderWidth={underline ? 0 : 1} borderRadius={5}>
    {items &&
      items.map((item, index) => {
        let isActive = false
        if (selectedIndex !== undefined && selectedIndex === index) isActive = true

        return (
          <TouchableItem
            onPress={() => onChange(index)}
            key={item.id || item}
            underline={underline}
            active={isActive}
            activeColor={activeColor}
          >
            <Text
              color={
                (isActive && underline && 'accent1') || (isActive && 'white') || (underline && 'shade7') || undefined
              }
            >
              {item.value || item}
            </Text>
            {underline && <Underline activeColor={activeColor} active={isActive} />}
          </TouchableItem>
        )
      })}
  </Box>
)

const TouchableItem = styled.TouchableOpacity<StateProps>(({ theme, underline, active }) => [
  {
    flex: 1,
    textAlign: 'center',
    verticalAlign: 'middle',
    paddingVertical: theme.space[2],
    borderBottomWidth: 0,
    backgroundColor: 'transparent',
  },
  underline && {
    borderBottomWidth: 0.5,
    borderBottomColor: theme.colors.shade7,
  },
  active && {
    backgroundColor: theme.colors.accent1,
  },
  underline &&
    active && {
      backgroundColor: theme.colors.transparent,
      borderBottomWidth: 2,
      borderBottomColor: theme.colors.accent1,
    },
])
