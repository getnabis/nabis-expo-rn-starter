import * as React from 'react'
import { Box, H1, H4, Screen, ScrollView, Text } from '../system/components'
import { BigRoundedButton } from './BigRoundedButton'

export default class NoResults extends React.Component {
  render() {
    return (
      <Screen>
        <ScrollView flex={1}>
          <H1>Saved</H1>
          <H4 muted>Not every day is filled with adventures, but you can start planning for the next one.</H4>
          <Text muted>Tap the heart on any home to start saving your favorites here.</Text>
        </ScrollView>
        <Footer>
          <BigRoundedButton title="Search" type="gradient" />
        </Footer>
      </Screen>
    )
  }
}

const Footer = Box
Footer.defaultProps = {
  borderTop: '1px solid',
  borderColor: 'ui2',
  position: 'absolute',
  height: 80,
  bottom: 0,
  left: 0,
  right: 0,
  px: 3,
}
