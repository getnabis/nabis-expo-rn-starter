import * as React from 'react'
import { isIphoneX } from 'react-native-iphone-x-helper'

import { Box, Touchable } from '../system'
import { Icon } from './Icon'

let buttonSize = 60
let buttonWrapperPadding = 0

if (!isIphoneX) {
  buttonSize = 50
  buttonWrapperPadding = 20
}

interface NextArrowButtonProps {
  disabled: boolean
  handleNextButton: () => any
}

export const NextArrowButton = ({ disabled, handleNextButton }: NextArrowButtonProps) => {
  return (
    <Box alignItems="flex-end" right={20} bottom={20} pt={buttonWrapperPadding}>
      <Touchable.Highlight
        onPress={handleNextButton}
        disabled={disabled}
        size={buttonSize}
        bg="shade9"
        opacity={disabled ? 0.2 : 0.6}
      >
        <Box size={buttonSize} center middle>
          <Icon type="feather" name="chevron-right" color="shade9" marginRight={-2} marginTop={-2} size={32} />
        </Box>
      </Touchable.Highlight>
    </Box>
  )
}
