import * as React from 'react'

import { Dimensions, ScrollView, StyleSheet, View } from 'react-native'

const { width: windowWidth, height: windowHeight } = Dimensions.get('window')

interface Props {
  horizontal: boolean
  pages: React.Component[]
  initialIndex: number
}

export class PageSwiper extends React.Component<Props> {
  static defaultProps: Props = {
    pages: [],
    horizontal: true,
    initialIndex: 0,
  }

  scrollView!: ScrollView

  componentDidMount() {
    if (this.props.horizontal) {
      const offset = windowWidth * this.props.initialIndex
      this.scrollView.scrollTo({ x: offset, animated: false })
    } else {
      const offset = windowHeight * this.props.initialIndex
      this.scrollView.scrollTo({ y: offset, animated: false })
    }
  }

  renderPages = () => {
    const { horizontal, pages } = this.props
    const itemStyle = horizontal ? styles.item : styles.verticalItem

    return pages.map((page, index) => {
      return (
        <View key={index} style={itemStyle}>
          {page}
        </View>
      )
    })
  }

  render() {
    const { horizontal } = this.props
    const scrollViewStyle = horizontal ? styles.scrollView : styles.scrollViewVertical

    return (
      <View style={styles.container}>
        <ScrollView
          ref={(c) => {
            this.scrollView = c
          }}
          horizontal={horizontal}
          pagingEnabled={true}
          showsHorizontalScrollIndicator={false}
          showsVerticalScrollIndicator={false}
          style={scrollViewStyle}
          scrollEventThrottle={32}
          bounces={false}
          directionalLockEnabled={true}
        >
          {this.renderPages()}
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  scrollView: {
    flexDirection: 'row',
  },
  scrollViewVertical: {
    flexDirection: 'column',
  },
  item: {
    flex: 1,
    width: windowWidth,
  },
  verticalItem: {
    flex: 1,
    height: windowHeight,
    width: windowWidth,
  },
})
