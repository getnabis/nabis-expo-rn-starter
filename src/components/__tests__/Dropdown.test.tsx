import 'jest-styled-components'
import * as React from 'react'
import 'react-native'
import renderer from 'react-test-renderer'
import { Dropdown } from '../Dropdown'

it('renders correctly', () => {
  const tree = renderer
    .create(
      <Dropdown
        data={[
          {
            value: 'item1',
            label: 'Item 1',
            props: {},
          },
          {
            value: 'item2',
            label: 'Item 2',
            props: {},
          },
          {
            value: 'item3',
            label: 'Item 3',
            props: {},
          },
        ]}
        onChangeText={() => {}}
      />,
    )
    .toJSON()

  expect(tree).toMatchSnapshot()
})
