import 'jest-styled-components'
import * as React from 'react'
import 'react-native'
import renderer from 'react-test-renderer'

import { SegmentedControl } from '../SegmentedControl'

it('renders correctly', () => {
  const tree = renderer
    .create(<SegmentedControl values={['item 1', 'item 2', 'item 3']} selectedColor="accent1" />)
    .toJSON()

  expect(tree).toMatchSnapshot()
})
