import 'jest-styled-components'
import * as React from 'react'
import 'react-native'

import renderer from 'react-test-renderer'
import { GoogleSignInButton } from '../GoogleSignInButton'

it('renders correctly', () => {
  const tree = renderer.create(<GoogleSignInButton />).toJSON()

  expect(tree).toMatchSnapshot()
})
