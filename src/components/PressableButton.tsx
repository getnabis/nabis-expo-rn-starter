import * as React from 'react'
import { TouchableWithoutFeedback } from 'react-native'
// import posed from 'react-native-pose'

import { LinearGradient } from './LinearGradient'
import { Spinner } from './Spinner'

import { useState } from '../hooks'
import { styled, Text, withTheme, WithTheme } from '../system'

// const ButtonAnimator = posed.View({
//   pressable: true,
//   init: {
//     transition: { type: 'spring', stiffness: 500 },
//     scale: 1,
//   },
//   press: {
//     scale: 0.95,
//   },
// })

const ShadowWrapper = styled.View({
  shadowOffset: { height: 3, width: 0 },
  shadowOpacity: 0.2,
  shadowColor: '#000000',
  shadowRadius: 2,
})

const ButtonOuter = styled.View({
  height: 60,
  borderRadius: 10,
  overflow: 'hidden',
})

const ButtonInner = styled.View({
  height: 60,
  justifyContent: 'center',
  alignItems: 'center',
})

interface Props {
  title: string
}

export const PressableButton = withTheme(({ title = '', theme }: WithTheme<Props>) => {
  const [isPressing, setPressing] = useState(false)

  const [loading, setLoading] = useState(false)

  return (
    <TouchableWithoutFeedback
      onPressIn={() => setPressing(true)}
      onPress={() => setLoading(!loading)}
      onPressOut={() => setPressing(false)}
    >
      {/* <ButtonAnimator pose={isPressing ? 'press' : 'init'}> */}
      <ShadowWrapper>
        <ButtonOuter>
          <LinearGradient colors={theme.gradients.default} start={[0, 0]} end={[1, 1]}>
            <ButtonInner>
              {!loading && <Text>{title}</Text>}
              {loading && <Spinner />}
            </ButtonInner>
          </LinearGradient>
        </ButtonOuter>
      </ShadowWrapper>
      {/* // </ButtonAnimator> */}
    </TouchableWithoutFeedback>
  )
})
