import * as React from 'react'
import { Box, Color, Flex, Small } from '../system'
import { Icon } from './Icon'

interface Props {
  votes: number
  size: number
  color: Color
}

export const Stars: React.FC<Props> = ({ votes, size, color }) => {
  const starElements = []
  for (let i = 0; i < 5; i++) {
    starElements.push(
      <Box center mr={1}>
        <Icon type="feather" key={`star-${i}`} name="star" size={size} color={votes > i ? color : 'shade2'} />
      </Box>,
    )
  }

  return (
    <Flex center>
      {starElements}
      {votes && (
        <Small fontSize={[0, 1]} fontWeight="bold">
          {votes}
        </Small>
      )}
    </Flex>
  )
}
