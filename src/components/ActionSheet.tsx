import * as React from 'react'
import { ActionSheetIOS, Dimensions, FlatList, Modal, Platform, TouchableOpacity } from 'react-native'

import { Col, Row, Text, ThemedProps, Touchable, withTheme } from '../system'

import { Icon, IconProps } from './Icon'

interface Props extends ThemedProps {
  autoHide: boolean
  duration: any
}

interface State {
  items: Item[]
  title?: any
  message?: any
  destructiveButtonIndex?: any
  cancelButtonIndex?: any
  modalVisible: boolean
  callback: (index: number, ...props: any[]) => void
}

interface Item {
  text: string
  iconProps?: IconProps
}

const ListItem = Touchable.WithoutFeedback

ListItem.defaultProps = {
  borderBottom: 1,
  borderColor: 'shade7',
}

class ActionSheetContainerBase extends React.Component<Props, State> {
  static show(config: any, callback: any) {
    this.actionsheet._root.showActionSheet(config, callback)
  }

  static hide() {
    this.actionsheet._root.hideActionSheet()
  }

  static actionsheet: any

  constructor(props: Props) {
    super(props)
    this.state = {
      modalVisible: false,
      items: [],
      callback: () => {},
    }
  }

  showActionSheet = (config: any, callback: any) => {
    if (Platform.OS === 'ios') {
      if (typeof config.options[0] === 'object') {
        const options = config.options
        const filtered = options.map((item: any) => {
          return item.text
        })
        config.options = filtered
        ActionSheetIOS.showActionSheetWithOptions(config, callback)
      } else {
        ActionSheetIOS.showActionSheetWithOptions(config, callback)
      }
    } else {
      this.setState({
        items: config.options,
        title: config.title,
        message: config.message,
        destructiveButtonIndex: config.destructiveButtonIndex,
        cancelButtonIndex: config.cancelButtonIndex,
        modalVisible: true,
        callback,
      })
    }
  }

  hideActionSheet = () => {
    this.setState({ modalVisible: false })
  }

  componentDidMount() {
    if (!this.props.autoHide && this.props.duration) {
      console.warn(`It's not recommended to set autoHide false with duration`)
    }
  }

  render() {
    return (
      <Modal
        animationType={'fade'}
        transparent={true}
        visible={this.state.modalVisible}
        onRequestClose={() => {
          this.state.callback(this.state.cancelButtonIndex)
          this.setState({ modalVisible: false })
        }}
      >
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => {
            this.state.callback(this.state.cancelButtonIndex)
            this.setState({ modalVisible: false })
          }}
          style={{
            backgroundColor: 'rgba(0,0,0,0.4)',
            flex: 1,
            justifyContent: 'flex-end',
          }}
        >
          <TouchableOpacity
            activeOpacity={1}
            style={{
              backgroundColor: '#fff',
              minHeight: 56,
              height: this.state.items.length * 80,
              maxHeight: Dimensions.get('window').height / 2,
              padding: 15,
              elevation: 4,
            }}
          >
            {this.state.title ? <Text color="shade5">{this.state.title}</Text> : null}
            <FlatList
              style={{
                marginHorizontal: -15,
                marginTop: this.state.title ? 15 : 0,
              }}
              data={this.state.items}
              keyExtractor={(_, index) => String(index)}
              renderItem={({ index, item }) => {
                return this.state.items && this.state.items[0] && typeof this.state.items[0] === 'string' ? (
                  <ListItem
                    onPress={() => {
                      this.state.callback(index)
                      this.setState({ modalVisible: false })
                    }}
                    style={{ borderColor: 'transparent', marginLeft: 14 }}
                  >
                    <Text>{item}</Text>
                  </ListItem>
                ) : (
                  <ListItem
                    onPress={() => {
                      this.state.callback(index)
                      this.setState({ modalVisible: false })
                    }}
                    ml={3}
                    height={50}
                    borderColor="transparent"
                  >
                    <Row>
                      <Col width={5} center middle>
                        {item.iconProps && <Icon {...item.iconProps} />}
                      </Col>
                      <Col pl={2}>
                        <Text>{item.text}</Text>
                      </Col>
                      <Col />
                    </Row>
                  </ListItem>
                )
              }}
            />
          </TouchableOpacity>
        </TouchableOpacity>
      </Modal>
    )
  }
}

export const ActionSheetContainer = withTheme(ActionSheetContainerBase)
