import * as React from 'react'

import { connectActionSheet } from '@expo/react-native-action-sheet'

import {
  HeaderButton as RNHeaderButton,
  HeaderButtonProps as RNHeaderButtonProps,
  HeaderButtons as RNHeaderButtons,
  HeaderButtonsProps as RNHeaderButtonsProps,
  onOverflowMenuPressParams,
} from 'react-navigation-header-buttons'

import { withTheme } from 'styled-components/native'
import { ThemedProps } from '../system'
import { Icon, IconProps } from './Icon'

/**
 * HeaderButton
 */

export interface HeaderButtonProps extends RNHeaderButtonProps, ThemedProps {}

export const HeaderButton = withTheme(({ theme, buttonStyle, ...props }: HeaderButtonProps) => (
  <RNHeaderButton IconComponent={Icon} iconSize={23} buttonStyle={buttonStyle} {...props} />
))

/**
 * HeaderButtons
 */

export interface HeaderButtonsProps extends RNHeaderButtonsProps {
  iconProps?: IconProps
}

export const HeaderButtons: React.FC<HeaderButtonsProps> = ({ iconProps, ...props }) => (
  <RNHeaderButtons
    HeaderButtonComponent={HeaderButton}
    OverflowIcon={<Icon size={23} type="feather" name="more-vertical" {...iconProps} />}
    {...props}
  />
)

/**
 * HeaderButtonItem
 */

export const HeaderButtonItem = RNHeaderButtons.Item

/**
 * Right HeaderButtons (withoverflow capabilities)
 */

export const RightHeaderButtons = connectActionSheet(props => {
  const onOpenActionSheet = ({ hiddenButtons }: onOverflowMenuPressParams) => {
    props.showActionSheetWithOptions(
      {
        options: hiddenButtons.map((hB: any) => hB.props.title),
        destructiveButtonIndex: 1,
      },
      (buttonIndex: number) => {
        // @ts-ignore
        hiddenButtons[buttonIndex].props.onPress()
      },
    )
  }
  return <HeaderButtons onOverflowMenuPress={onOpenActionSheet} {...props} />
})
