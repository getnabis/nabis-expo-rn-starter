import * as React from 'react'
import { Color, Touchable } from '../system'
import { Icon } from './Icon'

interface Props extends Touchable.OpacityProps {
  color: Color
  selectedColor: Color
  selected?: boolean
}

export const HeartButton: React.FC<Props> = ({ color, selectedColor, selected, ...props }) => (
  <Touchable.Opacity {...props}>
    <Icon type="feather" name={selected ? 'heart' : 'heart-o'} color={selected ? selectedColor : color} size={18} />
    <Icon
      type="feather"
      position="absolute"
      left={0}
      top={0}
      name="heart-o"
      size={18}
      color={color}
      style={[{ display: selected ? 'flex' : 'none' }]}
    />
  </Touchable.Opacity>
)
