import * as React from 'react'
import { Animated, ScrollView } from 'react-native'
import { DrawerItems, DrawerItemsProps, SafeAreaView } from 'react-navigation'

export interface DrawerComponentProps extends DrawerItemsProps {
  drawerOpenProgress?: any // fix typescript
}

export const DrawerComponent: React.FC<DrawerComponentProps> = props => {
  const translateX = props.drawerOpenProgress.interpolate({
    inputRange: [0, 1],
    outputRange: [-100, 0],
  })

  return (
    <Animated.View style={{ transform: [{ translateX }] }}>
      <ScrollView>
        <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always', horizontal: 'never' }}>
          <DrawerItems {...props} />
        </SafeAreaView>
      </ScrollView>
    </Animated.View>
  )
}
