import * as React from 'react'
import { Platform, ViewProps } from 'react-native'
import { borderColor, BorderColorProps } from 'styled-system'
import { Col, Color, Row, styled, Text } from '../system'

export interface SegmentedControlProps extends ViewProps {
  underline?: boolean
  values?: any[]
  selectedIndex?: number
  selectedColor?: Color
  onChange?: (index: number) => void
}

export const SegmentedControl: React.FC<SegmentedControlProps> = ({
  underline,
  values,
  selectedIndex,
  selectedColor,
  onChange,
  style,
}) => (
  <Row style={style}>
    <Col center mb={5}>
      {values.map((value, index) => (
        <Segment
          underline={underline}
          key={value}
          value={value}
          isSelected={index === selectedIndex}
          selectedColor={selectedColor}
          onPress={() => onChange(index)}
        />
      ))}
    </Col>
  </Row>
)

export interface SegmentProps extends BorderColorProps {
  isSelected?: boolean
  onPress?: () => void
  selectedColor?: Color
  value?: any
  underline?: boolean
}

export const Segment: React.FC<SegmentProps> = ({ isSelected, onPress, selectedColor, value, underline }) => {
  return (
    <SegmentTouchable
      accessibilityTraits={isSelected ? 'selected' : 'button'}
      activeOpacity={0.8}
      onPress={onPress}
      underline={underline}
      borderColor={isSelected && selectedColor}
    >
      <Text color="shade9" muted={!isSelected} opacity={0.82}>
        {value && value.toUpperCase()}
      </Text>
    </SegmentTouchable>
  )
}

const HEIGHT = 32

const SegmentTouchable = styled.TouchableOpacity(
  {
    justifyContent: 'center',
    alignItems: 'center',
  },
  borderColor,
  (props: SegmentProps) =>
    props.underline
      ? {
          paddingBottom: 6,
          paddingHorizontal: 10,
          borderBottomWidth: 2,
          marginRight: 10,
        }
      : Platform.select({
          ios: {
            height: HEIGHT,
            paddingHorizontal: 20,
            borderRadius: HEIGHT / 2,
            borderWidth: 1,
          },
          android: {
            paddingBottom: 6,
            paddingHorizontal: 10,
            borderBottomWidth: 3,
            marginRight: 10,
          },
        }),
)
