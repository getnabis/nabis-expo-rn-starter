import * as React from 'react'

import { Feather, FontAwesome5, Ionicons, MaterialIcons } from '@expo/vector-icons'
import { ViewProps } from 'react-native'

import { color, ColorProps, size, SizeProps, space, SpaceProps } from 'styled-system'

import { positions, PositionsProps, styled, withTheme, WithTheme } from '../system'
import { getIconName, IconNameAccessor } from './iconNames'

const iconTypes = {
  awesome5: FontAwesome5,
  feather: Feather,
  ionicon: Ionicons,
  material: MaterialIcons,
}

export type IconType = keyof typeof iconTypes

export interface IconProps extends PositionsProps, SizeProps, SpaceProps, ViewProps, ColorProps {
  name?: IconNameAccessor | string
  type?: IconType
}

const IconBase = withTheme(({ name, type, ...props }: WithTheme<IconProps>) => {
  const IconKind = iconTypes[type]
  return <IconKind name={typeof name === 'string' ? name : getIconName(name)} {...props} />
})

export const Icon = styled(IconBase)(positions, size, color, space)

Icon.displayName = 'Icon'
Icon.defaultProps = {
  type: 'feather',
  name: 'circle',
  size: 2,
  color: 'shade3',
}
