import * as React from 'react'
import { Text, TouchableOpacity, View } from 'react-native'

import { ThemedProps, withTheme } from '../system'

interface Props extends ThemedProps {
  title?: string
  icon?: object
  onPress?: () => any
  location?: 'right' | 'left'
  color?: string
}

export const NavBarButton = withTheme(({ title, icon, onPress }: Props) => {
  let content

  if (title) {
    content = (
      <View>
        <Text>{title}</Text>
        {icon}
      </View>
    )
  } else if (icon) {
    content = <View>{icon}</View>
  }

  return <TouchableOpacity onPress={onPress}>{content}</TouchableOpacity>
})
