// inspired by @flatlogic/react-native-starer

import * as React from 'react'
import {
  ActivityIndicator,
  Image,
  PixelRatio,
  StyleProp,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  ViewStyle,
} from 'react-native'

import { Color, getColor, themeGet, withTheme, WithTheme } from '../system'
import { LinearGradient } from './LinearGradient'
import { Spinner } from './Spinner'

interface Props {
  caption?: string
  bordered?: boolean
  primary?: boolean
  small?: boolean
  secondary?: boolean
  bgColor?: Color
  icon?: any
  rounded?: boolean
  textColor?: Color
  loading?: boolean
  large?: boolean
  children?: React.ReactChildren
  action?: any
  onPress?: () => void
  style?: StyleProp<ViewStyle>
  bgGradientStart?: string
  bgGradientEnd?: string
}

const borderRadius = 40

export const Button = withTheme((props: WithTheme<Props>) => {
  const caption = props.caption && props.caption.toUpperCase()
  let iconImage

  if (props.icon) {
    iconImage = <Image resizeMode="contain" source={props.icon} style={styles.icon} />
  }

  let content

  if (props.bordered) {
    const borderedStyle = [
      styles.button,
      props.small && styles.buttonSmall,
      styles.border,
      props.primary && {
        borderColor: getColor('accent1')(props),
      },
      props.secondary && {
        borderColor: getColor('accent2')(props),
      },
      props.bgColor && {
        borderColor: getColor(props.bgColor)(props),
      },
      props.rounded && styles.rounded,
    ]

    const textStyle = [
      styles.caption,
      props.small && styles.captionSmall,
      styles.secondaryCaption,
      iconImage && styles.captionWithIcon,
      props.primary && {
        color: getColor('accent1')(props),
      },
      props.secondary && {
        color: getColor('accent2')(props),
      },
      props.bgColor && {
        color: getColor(props.bgColor)(props),
      },
      props.textColor && {
        color: props.textColor,
      },
    ]

    content = (
      <View style={borderedStyle}>
        {iconImage && <View>{iconImage}</View>}
        {props.loading && <ActivityIndicator color="white" />}
        {!props.loading && props.caption && <Text style={textStyle}>{caption}</Text>}
        {props.children && props.children}
      </View>
    )
  } else {
    const isPrimary = props.primary || (!props.primary && !props.secondary)
    let gradientArray =
      props.bgGradientStart && props.bgGradientEnd ? [props.bgGradientStart, props.bgGradientEnd] : undefined

    if (!gradientArray) {
      gradientArray = isPrimary
        ? themeGet(t => t.gradients.default)(props)
        : themeGet(t => t.gradients.secondary)(props)
    }

    if (props.bgColor) {
      const bgColorColor = getColor(props.bgColor)(props)
      gradientArray = [bgColorColor, bgColorColor]
    }

    content = (
      <LinearGradient
        start={[0, 0]}
        end={[1, 1]}
        colors={gradientArray}
        style={[
          styles.button,
          props.small && styles.buttonSmall,
          styles.primaryButton,
          props.rounded && { borderRadius },
          props.action && styles.action,
        ]}
      >
        {iconImage && iconImage}
        {props.loading && <Spinner color="white" />}
        {!props.loading && props.caption && (
          <Text
            style={[
              styles.caption,
              props.small && styles.captionSmall,
              iconImage && styles.captionWithIcon,
              styles.primaryCaption,
            ]}
          >
            {caption}
          </Text>
        )}
        {!props.loading && props.children && props.children}
      </LinearGradient>
    )
  }

  return (
    <TouchableOpacity
      accessibilityTraits="button"
      onPress={props.onPress}
      activeOpacity={0.8}
      style={[
        styles.container,
        props.small && styles.containerSmall,
        props.large && styles.containerLarge,
        props.style,
      ]}
    >
      {content}
    </TouchableOpacity>
  )
})

const HEIGHT = 40
const HEIGHT_SMALL = 30
const HEIGHT_LARGE = 50

const styles = StyleSheet.create({
  container: {
    height: HEIGHT,
    borderWidth: 1 / PixelRatio.get(),
  },
  containerSmall: {
    height: HEIGHT_SMALL,
  },
  containerLarge: {
    height: HEIGHT_LARGE,
  },
  button: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 30,
  },
  buttonSmall: {
    paddingHorizontal: 20,
  },
  border: {
    borderWidth: 1,
    borderColor: 'white',
    borderRadius: 5,
  },
  primaryButton: {
    backgroundColor: 'transparent',
    borderRadius: 5,
  },
  rounded: {
    borderRadius: HEIGHT_LARGE / 2,
  },
  icon: {
    maxHeight: HEIGHT - 20,
    maxWidth: HEIGHT - 20,
  },
  caption: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  captionSmall: {
    fontSize: 12,
    fontWeight: '500',
  },
  captionWithIcon: {
    marginLeft: 12,
  },
  primaryCaption: {
    color: 'white',
  },
  secondaryCaption: {
    color: 'white',
    backgroundColor: 'transparent',
  },
  action: {
    borderRadius: 20,
    height: HEIGHT,
    width: HEIGHT,
    paddingHorizontal: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
