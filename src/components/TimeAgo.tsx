import moment from 'moment'
import React, { Component } from 'react'

import { Text } from '../system'

interface Props {
  time: number | string | Date
  interval: number
  hideAgo: boolean
}

interface State {
  intervalId: any
}

export default class TimeAgo extends Component<Props, State> {
  static defaultProps = {
    hideAgo: false,
    interval: 60000,
  }

  componentDidMount() {
    const intervalId = setInterval(this.timer, this.props.interval)
    this.setState({ intervalId })
  }

  componentWillUnmount() {
    clearInterval(this.state.intervalId)
  }

  timer = () => {
    this.forceUpdate()
  }

  render() {
    const { time, hideAgo } = this.props
    const timestamp = time instanceof Date ? time : new Date(time)
    return <Text {...this.props}>{moment(timestamp).fromNow(hideAgo)}</Text>
  }
}
