import { Box, BoxProps } from '../system/components'

export interface CardProps extends BoxProps {}

export const Card = Box

Card.displayName = 'Card'
Card.defaultProps = {
  color: 'shade7',
  boxShadow: 3,
}
