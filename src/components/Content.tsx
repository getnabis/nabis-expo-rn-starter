import { Box } from '../system'

export const Content = Box

Content.displayName = 'Content'
Content.defaultProps = {
  mx: [2, 3],
  flex: 1,
}
