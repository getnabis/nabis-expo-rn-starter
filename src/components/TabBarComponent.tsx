import * as React from 'react'
import { BottomTabBar } from 'react-navigation'

import { withTheme } from '../system'

export const TabBarComponent = withTheme(({ theme, ...props }) => {
  return <BottomTabBar {...props} />
})
