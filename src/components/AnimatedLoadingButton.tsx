import * as React from 'react'
import { TouchableOpacity } from 'react-native'
// import posed from 'react-native-pose'
import { Box, Flex, Text } from '../system/components'
import { Spinner } from './Spinner'

// const ButtonWrapper = posed.View({
//   default: { scaleX: 1, opacity: 1 },
//   loading: { scaleX: 0, opacity: 0 },
// })

// const LoadingWrapper = posed.View({
//   default: { opacity: 0, scaleX: 0, scaleY: 0 },
//   loading: { opacity: 1, scaleX: 1, scaleY: 1 },
// })

interface Props {
  onPress: () => void
  loading: boolean
  renderButton: () => React.ElementType<any>
  renderLoading: () => React.ElementType<any>
}

export const AnimatedLoadingButton: React.FC<Props> = props => {
  const { loading, onPress } = props
  const animationState = loading ? 'loading' : 'default'
  return (
    <TouchableOpacity onPress={onPress} disabled={loading}>
      <Box center middle>
        {/* <ButtonWrapper pose={animationState}> */}
        <Flex>
          <Text color="shade5">Press Me</Text>
        </Flex>
        {/* </ButtonWrapper> */}
        <Box position="absolute">
          {/* <LoadingWrapper pose={animationState}> */}
          <Flex>
            <Spinner color="shade5" />
          </Flex>
          {/* </LoadingWrapper> */}
        </Box>
      </Box>
    </TouchableOpacity>
  )
}
