import { BlurView } from 'expo'
import * as React from 'react'
import { Animated, StyleSheet } from 'react-native'

import { Box, Image } from '../system'

const AnimatedBlurView = Animated.createAnimatedComponent(BlurView)

interface Props {
  backgroundImageUri?: string
}

interface State {
  intensity: Animated.Value
}

export class AnimatedBlurryView extends React.Component<Props, State> {
  state = {
    intensity: new Animated.Value(0),
  }

  componentDidMount() {
    this._animate()
  }

  _animate = () => {
    const { intensity } = this.state
    Animated.timing(intensity, { duration: 2500, toValue: 100 }).start(() => {
      Animated.timing(intensity, { duration: 2500, toValue: 0 }).start(this._animate)
    })
  }

  render() {
    return (
      <Box flex={1}>
        <AnimatedBlurView tint="default" intensity={this.state.intensity} style={StyleSheet.absoluteFill}>
          {this.props.backgroundImageUri && <Image flex={1} source={{ uri: this.props.backgroundImageUri }} />}
        </AnimatedBlurView>
        {this.props.children}
      </Box>
    )
  }
}
