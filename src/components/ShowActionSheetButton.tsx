import * as React from 'react'

import { Entypo } from '@expo/vector-icons'
import { ActionSheetIOSOptions, StyleProp, TextStyle, ViewStyle } from 'react-native'

import { Box, Text } from '../system'
import { Icon } from './Icon'
import { IconButton } from './IconButton'

interface ActionSheetAndroidOptions {
  icons?: JSX.Element[]
  tintIcons?: boolean // default is true
  showSeparators?: boolean // defualt is false
  textStyle?: StyleProp<ViewStyle>
  titleTextStyle?: StyleProp<TextStyle>
  messageTextStyle?: StyleProp<TextStyle>
}

interface ActionSheetOptions extends ActionSheetIOSOptions, ActionSheetAndroidOptions {}

interface ShowActionSheetButtonProps {
  title: string
  onSelection?: (index: number) => void
  showActionSheetWithOptions: (options: ActionSheetOptions, callback: (buttonIndex: number) => void) => void
  withTitle?: boolean
  withMessage?: boolean
  withIcons?: boolean
  withSeparators?: boolean
  withCustomStyles?: boolean
}

const icon = (name: string) => <Icon type="material" key={name} name={name} size={24} />

// A custom button that shows examples of different share sheet configurations
export class ShowActionSheetButton extends React.PureComponent<ShowActionSheetButtonProps> {
  _showActionSheet = () => {
    const {
      withTitle,
      withMessage,
      withIcons,
      withSeparators,
      withCustomStyles,
      onSelection,
      showActionSheetWithOptions,
    } = this.props

    // Same interface as https://facebook.github.io/react-native/docs/actionsheetios.html
    const options = ['Delete', 'Save', 'Share', 'Cancel']
    const icons = withIcons ? [icon('delete'), icon('save'), icon('share'), icon('cancel')] : null
    const title = withTitle ? 'Choose An Action' : null
    const message = withMessage ? 'This library tries to mimic the native share sheets as close as possible.' : null
    const destructiveButtonIndex = 0
    const cancelButtonIndex = 3

    const textStyle: TextStyle = withCustomStyles ? { fontSize: 20, fontWeight: '500', color: 'blue' } : null

    const titleTextStyle: TextStyle = withCustomStyles && {
      fontSize: 24,
      textAlign: 'center',
      fontWeight: '700',
      color: 'orange',
    }
    const messageTextStyle: TextStyle = withCustomStyles && { fontSize: 12, color: 'purple', textAlign: 'right' }

    showActionSheetWithOptions(
      {
        options,
        cancelButtonIndex,
        destructiveButtonIndex,
        title,
        message,
        icons,
        tintIcons: true,
        showSeparators: withSeparators,
        textStyle,
        titleTextStyle,
        messageTextStyle,
      },
      (buttonIndex: number) => {
        onSelection(buttonIndex)
      },
    )
  }

  render() {
    const { title } = this.props
    return (
      <Box m={2}>
        <Entypo.Button name="code" bg="#efefef" onPress={this._showActionSheet}>
          <Text
            style={{
              fontSize: 15,
              color: '#fff',
            }}
          >
            {title}
          </Text>
        </Entypo.Button>
        <IconButton iconProps={{ name: 'code' }} bg="shade7" onPress={this._showActionSheet}>
          <Text fontSize={2} color="white">
            {title}
          </Text>
        </IconButton>
      </Box>
    )
  }
}
