import { BlurView } from 'expo'
import React from 'react'
import { Image, StyleSheet, View } from 'react-native'

interface Props {
  children?: React.ReactNode
  backgroundImageUri?: string
}

interface State {}

export class BlurryView extends React.Component<Props, State> {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <BlurView tint="light" intensity={50} style={StyleSheet.absoluteFill}>
          {this.props.backgroundImageUri && (
            <Image style={StyleSheet.absoluteFill} source={{ uri: this.props.backgroundImageUri }} />
          )}
        </BlurView>
        {this.props.children}
      </View>
    )
  }
}
