import * as React from 'react'
import { StyleProp, TextProps, ViewStyle } from 'react-native'
import { Color, Row, Text, Touchable } from '../system'

import { Icon, IconProps } from './Icon'

interface ButtonProps {
  iconProps?: IconProps
  textProps?: TextProps
  buttonStyle?: StyleProp<ViewStyle>
  title?: string
  children?: React.ReactNode
  activeOpacity?: number
  style?: StyleProp<ViewStyle>
  disabled?: boolean
  text?: boolean
  type?: 'text' | 'default'
}

export const StandardButton: React.FC<ButtonProps> = ({
  iconProps,
  textProps,
  buttonStyle,
  title,
  children,
  activeOpacity = 0.9,
  style,
  text = false,
  disabled,
  ...props
}) => {
  const textColor: Color[] = text ? ['accent1', 'shade7'] : ['white', 'white80']
  const buttonColor: Color[] = text ? ['transparent', 'transparent'] : ['accent1', 'shade7']
  return (
    <Touchable.Opacity
      bg={!disabled ? buttonColor[0] : buttonColor[1]}
      activeOpacity={activeOpacity}
      style={buttonStyle}
      {...props}
    >
      <Row center middle style={style}>
        {iconProps && <Icon size="regular" mr={2} {...iconProps} />}
        {title && (
          <Text color={!disabled ? textColor[0] : textColor[1]} {...textProps}>
            {title}
          </Text>
        )}
        {children}
      </Row>
    </Touchable.Opacity>
  )
}
