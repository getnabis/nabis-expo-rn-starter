import * as React from 'react'
import { NavigationScreenProps } from 'react-navigation'

import { H1, P, Screen } from '../system'

export interface AppProps extends NavigationScreenProps {}
export interface AppState {}

export default class SignUpScreen extends React.Component<AppProps, AppState> {
  state = {}

  render() {
    return (
      <Screen safearea center middle>
        <H1>SignUpScreen</H1>
        <P>Welcome to the App screen!</P>
      </Screen>
    )
  }
}
