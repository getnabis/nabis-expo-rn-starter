import * as React from 'react'
import { useMutation } from 'react-apollo-hooks'
import { KeyboardAvoidingView, SafeAreaView } from 'react-native'
import { isEmail, isEmpty, normalizeEmail } from 'validator'

import { BigRoundedButton, FormField, TextButton } from '../../components'
import { SIGNUP_MUTATION } from '../../graphql/mutations/auth'
import { useForm } from '../../hooks'
import { i18n } from '../../i18n'
import Logger from '../../services/Logger'
import { navigateTo } from '../../services/Navigation'

import { Box, H1, Screen, Text } from '../../system'

interface FormValues {
  firstName: string
  lastName: string
  email: string
  password: string
  password2: string
}

export const SignInForm = () => {
  const signUp = useMutation(SIGNUP_MUTATION)

  const { errors, touched, values, handleBlur, handleChange, handleSubmit, isSubmitting } = useForm<FormValues>({
    initialValues: {
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      password2: '',
    },
    onSubmit: ({ firstName, lastName, email, password }: any) => {
      signUp({
        variables: { firstName, lastName, email, password },
      })
        .then(({ data }) => {
          Logger.info({ message: 'Sign Up Success!', data })
        })
        .catch(err => {
          Logger.error(err)
        })
    },
    validate: ({ firstName, lastName, email, password, password2 }: any) => ({
      email: isEmpty(email) || !isEmail(email) ? i18n.t('emailValidError') : undefined,
      firstName: isEmpty(firstName) ? i18n.t('firstNameValidError') : undefined,
      lastName: isEmpty(lastName) ? i18n.t('lastNameValidError') : undefined,
      password: isEmpty(password) ? i18n.t('passwordValidError') : undefined,
      password2: isEmpty(password2)
        ? i18n.t('passwordConfirmValidError')
        : !isEmpty(password) && !isEmpty(password2) && password !== password2
        ? i18n.t('passwordConfirmMatchError')
        : undefined,
    }),
  })

  const hasErrors: boolean = Object.keys(errors).length > 0

  return (
    <Screen>
      <SafeAreaView>
        <KeyboardAvoidingView>
          <H1>Sign Up</H1>
          <Box>
            <FormField
              error={touched.firstName && errors.firstName}
              label="Name"
              name="name"
              onBlur={handleBlur}
              onChange={handleChange}
              returnKeyType="next"
              textContentType="name"
              value={values.name}
            />
            <FormField
              error={touched.email && errors.email}
              label="Email"
              name="email"
              onBlur={handleBlur}
              onChange={handleChange}
              onChangeText={normalizeEmail}
              returnKeyType="next"
              textContentType="emailAddress"
              value={values.email}
            />
            <FormField
              error={touched.password && errors.password}
              label="New Password"
              name="password"
              onBlur={handleBlur}
              onChange={handleChange}
              returnKeyType="next"
              textContentType="newPassword"
              value={values.password}
              secureTextEntry
            />
            <FormField
              error={touched.password2 && errors.password2}
              label="Re-enter Password"
              name="password2"
              onBlur={handleBlur}
              onChange={handleChange}
              returnKeyType="done"
              textContentType="newPassword"
              value={values.password2}
              secureTextEntry
            />
          </Box>
          <BigRoundedButton
            disabled={hasErrors || isSubmitting}
            loading={isSubmitting}
            onPress={handleSubmit}
            title="Sign Up"
          />
          <Text center my={2}>
            Or
          </Text>
          <Box>
            <Text>Already have an accout?</Text> <TextButton onPress={() => navigateTo('SignIn')}>Sign In</TextButton>
          </Box>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </Screen>
  )
}
