import * as React from 'react'
import { useMutation } from 'react-apollo-hooks'
import { isEmpty } from 'validator'

import { KeyboardAvoidingView, SafeAreaView } from 'react-native'
import { BigRoundedButton, FormField, TextButton } from '../../components'
import { LOGIN_MUTATION } from '../../graphql/mutations/auth'
import { useForm } from '../../hooks'
import { translate } from '../../i18n'
import Logger from '../../services/Logger'
import { navigateTo } from '../../services/Navigation'
import { Box, H1, Screen, Text } from '../../system'

interface FormValues {
  email: string
  password: string
}

export const LoginForm = () => {
  const login = useMutation(LOGIN_MUTATION)

  const { errors, touched, values, handleBlur, handleChange, handleSubmit, isSubmitting } = useForm<FormValues>({
    initialValues: {
      email: '',
      password: '',
    },
    onSubmit: ({ email, password }) => {
      login({
        variables: { email, password },
      })
        .then(({ data }) => {
          Logger.info({ message: 'Login Success!', data })
        })
        .catch(err => {
          Logger.error(err)
        })
    },
    validate: ({ email, password }) => ({
      email: isEmpty(email) ? translate(t => t.forms.errors.emailValid) : undefined,
      password: isEmpty(password) ? translate(t => t.forms.errors.passwordValid) : undefined,
    }),
  })

  const hasErrors: boolean = Object.keys(errors).length > 0

  return (
    <Screen>
      <SafeAreaView>
        <KeyboardAvoidingView>
          <H1>Login</H1>
          <Box>
            <FormField
              error={touched.email && errors.email}
              label="Email"
              name="email"
              onBlur={handleBlur}
              onChange={handleChange}
              returnKeyType="next"
              textContentType="emailAddress"
              value={values.email}
            />
            <FormField
              error={touched.password && errors.password}
              label="New Password"
              name="password"
              onBlur={handleBlur}
              onChange={handleChange}
              returnKeyType="next"
              textContentType="newPassword"
              value={values.password}
              secureTextEntry
            />
          </Box>
          <BigRoundedButton
            disabled={hasErrors || isSubmitting}
            loading={isSubmitting}
            onPress={handleSubmit}
            title="Sign In"
          />
          <Text center my={2}>
            Or
          </Text>
          <Box>
            <Text>Don't have an accout?</Text> <TextButton onPress={() => navigateTo('SignUp')}>Sign Up</TextButton>
          </Box>
        </KeyboardAvoidingView>
      </SafeAreaView>
    </Screen>
  )
}
