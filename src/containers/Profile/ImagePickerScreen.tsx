// docs: https://docs.expo.io/versions/latest/sdk/imagepicker/
import { ImagePicker } from 'expo'
import * as React from 'react'
import { Button, Image, View } from 'react-native'

import { NavigationScreenProps } from 'react-navigation'
interface Props extends NavigationScreenProps {}
interface State {
  imageUri?: string
}

export class ImagePickerScreen extends React.Component<Props, State> {
  render() {
    const { imageUri } = this.state

    return (
      <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Button title="Pick an image from camera roll" onPress={this._pickImage} />
        {imageUri && <Image source={{ uri: imageUri }} style={{ width: 200, height: 200 }} />}
      </View>
    )
  }

  _pickImage = async () => {
    const result = await ImagePicker.launchImageLibraryAsync({
      allowsEditing: true,
      aspect: [4, 3],
    })

    console.log(result)

    if (result.cancelled === false && result.uri) {
      this.setState({ imageUri: result.uri })
    }
  }
}
