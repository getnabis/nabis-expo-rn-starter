import * as React from 'react'
import { NavigationScreenProps } from 'react-navigation'

import { H1, Screen, Text } from '../../system'

export interface EditProfileScreenProps extends NavigationScreenProps {}
export interface EditProfileScreenState {}

export default class EditProfileScreenScreen extends React.Component<EditProfileScreenProps, EditProfileScreenState> {
  state = {}

  render() {
    return (
      <Screen safearea center middle>
        <H1>EditProfileScreen</H1>
        <Text>Welcome to the EditProfileScreen screen!</Text>
      </Screen>
    )
  }
}
