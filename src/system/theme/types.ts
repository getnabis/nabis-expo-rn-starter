import { colors, gradients } from './colors'
import { defaultTheme } from './defaultTheme'
import { theme } from './theme'

export type FontFamily = keyof typeof defaultTheme.fonts
export type FontStyle = keyof typeof defaultTheme.fonts.default
export type FontWeight = keyof typeof defaultTheme.fonts.default.normal

export type Color = keyof typeof colors

export type ColorTheme = keyof typeof defaultTheme.modes
export type ThemeMode = keyof typeof defaultTheme.modes

export type GradientColor = keyof typeof gradients
export type DefaultTheme = typeof defaultTheme
export type Theme = typeof theme

export interface ThemedProps {
  theme: DefaultTheme
}

export type WithTheme<P> = P & ThemedProps
