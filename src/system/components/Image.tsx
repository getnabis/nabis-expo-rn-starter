import { ImageProps as RNImageProps } from 'react-native'
import styled from 'styled-components/native'
import { size, SizeProps } from 'styled-system'
import {
  basics,
  BasicsProps,
  borders,
  flexboxes,
  FlexboxesProps,
  positions,
  PositionsProps,
  space,
  SpaceProps,
} from '../styles'

export interface ImageProps extends BasicsProps, FlexboxesProps, PositionsProps, SpaceProps, SizeProps, RNImageProps {}

export const Image = styled.Image<ImageProps>(basics, flexboxes, positions, space, size, borders)
