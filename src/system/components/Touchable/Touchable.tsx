// import * as React from 'react'
import * as RN from 'react-native'
// import posed from 'react-native-pose'
import styled from 'styled-components/native'
import { compose } from 'styled-system'

import * as System from '../../styles'

const touchableProps = compose(
  System.basics,
  System.borders,
  System.dimensions,
  System.flexboxes,
  System.positions,
  System.space,
  System.typography,
)

interface TouchableStyledProps
  extends System.BasicsProps,
    System.BordersProps,
    System.DimensionsProps,
    System.FlexboxesProps,
    System.PositionsProps,
    System.SpaceProps,
    System.TypographyProps {}

export interface OpacityProps extends RN.TouchableOpacityProps, TouchableStyledProps {}
export interface HighlightProps extends RN.TouchableHighlightProps, TouchableStyledProps {}
export interface NativeFeedbackProps extends RN.TouchableNativeFeedbackProps, TouchableStyledProps {}
export interface WithoutFeedbackProps extends RN.TouchableWithoutFeedbackProps, TouchableStyledProps {}

export const Opacity = styled.TouchableOpacity<TouchableStyledProps>(touchableProps)
export const Highlight = styled.TouchableHighlight<TouchableStyledProps>(touchableProps)
export const NativeFeedback = styled.TouchableNativeFeedback<TouchableStyledProps>(touchableProps)
export const WithoutFeedback = styled.TouchableWithoutFeedback<TouchableStyledProps>(touchableProps)

// export const PressableView = posed.View({
//   pressable: true,
//   init: { scale: 1 },
//   press: { scale: 0.8 },
// })
