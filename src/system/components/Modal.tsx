import { Modal as RNModal, ModalProps as RNModalProps } from 'react-native'
import styled from 'styled-components/native'

import {
  basics,
  BasicsProps,
  borders,
  BordersProps,
  dimensions,
  DimensionsProps,
  flexboxes,
  FlexboxesProps,
  positions,
  PositionsProps,
  space,
  SpaceProps,
} from '../styles'

export interface ModalProps
  extends BasicsProps,
    BordersProps,
    DimensionsProps,
    FlexboxesProps,
    PositionsProps,
    SpaceProps,
    RNModalProps {
  center?: boolean
  middle?: boolean
  space?: 'between' | 'around' | 'evenly'
}

export const Modal = styled(RNModal)<ModalProps>(
  basics,
  borders,
  dimensions,
  flexboxes,
  positions,
  space,
  (props) => props.center && { alignItems: 'center' },
  (props) => props.middle && { justifyContent: 'center' },
  (props) => props.space && { justifyContent: `space-${props.space}` },
)
