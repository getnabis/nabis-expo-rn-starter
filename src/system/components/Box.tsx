import { View, ViewProps } from 'react-native'
import { styled } from '../system'

import {
  basics,
  BasicsProps,
  borders,
  BordersProps,
  dimensions,
  DimensionsProps,
  flexboxes,
  FlexboxesProps,
  positions,
  PositionsProps,
  space,
  SpaceProps,
} from '../styles'

export interface BoxProps
  extends BasicsProps,
    BordersProps,
    DimensionsProps,
    FlexboxesProps,
    PositionsProps,
    SpaceProps,
    ViewProps {
  center?: boolean
  middle?: boolean
  row?: boolean
  col?: boolean
  space?: 'between' | 'around' | 'evenly'
}

export const Box = styled(View)<BoxProps>(
  basics,
  borders,
  dimensions,
  flexboxes,
  positions,
  space,
  (props) => props.row && { flexDirection: 'row' },
  (props) => props.col && { flexDirection: 'column' },
  (props) => props.center && { alignItems: 'center' },
  (props) => props.middle && { justifyContent: 'center' },
  (props) => props.space && { justifyContent: `space-${props.space}` },
)
