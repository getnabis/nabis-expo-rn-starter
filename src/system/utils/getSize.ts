import { ThemedProps } from '../theme'

export const getSize = (size: number) => <P extends ThemedProps>(props: P, defaultSize = 2) =>
  props.theme.sizes[size] || defaultSize
