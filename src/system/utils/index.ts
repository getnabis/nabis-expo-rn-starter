export * from './getColor'
export * from './getSize'
export * from './getTheme'
export * from './themeGet'
