import { DefaultTheme } from '../theme'

export const themeGet = <T extends DefaultTheme>(accessor: (theme: T) => any) => <P extends { theme: T }>(props: P) =>
  accessor(props.theme)
