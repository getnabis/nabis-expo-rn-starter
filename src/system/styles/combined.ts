import { compose } from 'styled-system'
import { basics, BasicsProps } from './basics'
import { borders, BordersProps } from './borders'
import { dimensions, DimensionsProps } from './dimensions'
import { flexboxes, FlexboxesProps } from './flexboxes'
import { positions, PositionsProps } from './positions'
import { space, SpaceProps } from './space'
import { typography, TypographyProps } from './typography'

export interface CombinedProps
  extends BasicsProps,
    BordersProps,
    DimensionsProps,
    FlexboxesProps,
    PositionsProps,
    SpaceProps,
    TypographyProps {}

export const combined = compose(
  basics,
  borders,
  dimensions,
  flexboxes,
  positions,
  space,
  typography,
)
