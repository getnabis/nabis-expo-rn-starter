import { CSSObject } from 'styled-components'
import {
  borderBottom,
  BorderBottomProps,
  borderColor,
  BorderColorProps,
  borderLeft,
  BorderLeftProps,
  borderRadius,
  BorderRadiusProps,
  borderRight,
  BorderRightProps,
  borderStyle,
  BorderStyleProps,
  borderTop,
  BorderTopProps,
  borderWidth,
  BorderWidthProps,
  boxShadow,
  BoxShadowProps,
  compose,
} from 'styled-system'
import { Color, WithTheme } from '../theme'
import { getColor } from '../utils'

export interface BordersProps
  extends BorderStyleProps,
    BorderColorProps,
    BoxShadowProps,
    BorderBottomProps,
    BorderRightProps,
    BorderLeftProps,
    BorderTopProps,
    BorderRadiusProps,
    BorderWidthProps,
    BorderBottomColorProps {}

interface BorderBottomWidthProps {
  borderBottomWidth?: number
}
const borderBottomWidth = ({ borderBottomWidth }: BorderBottomWidthProps): CSSObject =>
  borderBottomWidth && { borderBottomWidth, borderBottomStyle: 'solid' }

interface BorderBottomColorProps {
  borderBottomColor?: Color
}
const borderBottomColor = ({ borderBottomColor, ...props }: WithTheme<BorderBottomColorProps>): CSSObject =>
  borderBottomColor && { borderBottomColor: getColor(borderBottomColor)(props) }

export const borders = compose(
  borderStyle,
  borderColor,
  boxShadow,
  borderTop,
  borderBottom,
  borderLeft,
  borderRight,
  borderRadius,
  borderWidth,
  borderBottomWidth,
  borderBottomColor,
)
