import {
  color,
  compose,
  fontSize,
  FontSizeProps,
  letterSpacing,
  LetterSpacingProps,
  lineHeight,
  LineHeightProps,
  textAlign,
  TextAlignProps,
} from 'styled-system'
import { defaultTheme, FontFamily, FontStyle, FontWeight, WithTheme } from '../theme'

export interface FontsStyleProps extends FontSizeProps {
  font?: FontFamily
  fontStyle?: FontStyle
  fontWeight?: FontWeight
}

export interface TypographyProps
  extends FontSizeProps,
    LetterSpacingProps,
    LineHeightProps,
    TextAlignProps,
    FontsStyleProps {
  muted?: boolean
}

const fontStyle = ({
  theme = defaultTheme,
  font = 'default',
  fontStyle = 'normal',
  fontWeight = 'normal',
}: WithTheme<FontsStyleProps>) => ({
  fontFamily: theme.fonts[font][fontStyle][fontWeight],
})

export const typography = compose(
  color,
  fontSize,
  letterSpacing,
  lineHeight,
  textAlign,
  fontStyle,
  (props) => props.muted && { opacity: 0.8 },
)
