import { SpaceProps as SSSpaceProps } from 'styled-system'

export { space } from 'styled-system'

export interface SpaceProps extends SSSpaceProps {}
