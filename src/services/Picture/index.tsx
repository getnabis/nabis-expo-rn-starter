import { getAuthToken } from '../Auth'

export const pickPicture = () => {
  return new Promise((resolve, reject) => {
    const input = document.createElement('input')
    input.type = 'file'
    input.accept = 'image/*'
    input.onchange = (e) => {
      const target = e.target as HTMLInputElement
      resolve(target.files[0])
    }
    input.onerror = reject
    input.click()
  })
}

export const uploadProfilePicture = async (file: any) => {
  const formData = new FormData()
  formData.append('file', file)
  formData.append('upload_preset', 'profile-pic')

  const authToken = await getAuthToken()

  // TODO: Need to change this to a Graphql mutation with image data

  return fetch(`${process.env.REACT_APP_SERVER_URL}/upload-profile-pic`, {
    method: 'POST',
    body: formData,
    headers: {
      Authorization: `Bearer ${authToken}`,
    },
  }).then((res) => {
    return res.json()
  })
}
