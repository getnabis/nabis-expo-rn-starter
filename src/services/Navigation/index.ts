import { DrawerActions, NavigationActions, NavigationParams } from 'react-navigation'

import { RouteName } from '../../navigation/Routes'

let _navigator: any

export function setTopLevelNavigator(navigatorRef: any) {
  _navigator = navigatorRef
}

export function navigateTo(routeName: RouteName, params?: NavigationParams) {
  _navigator.dispatch(
    NavigationActions.navigate({
      routeName,
      params,
    }),
  )
}

export function goBack() {
  _navigator.dispatch(NavigationActions.back())
}

export function openDrawer() {
  _navigator.dispatch(DrawerActions.openDrawer())
}

export function closeDrawer() {
  _navigator.dispatch(DrawerActions.closeDrawer())
}

export function closeScreen() {
  _navigator.dispatch(NavigationActions.back())
}

const NavigationService = {
  setTopLevelNavigator,
  navigateTo,
}

export default NavigationService
