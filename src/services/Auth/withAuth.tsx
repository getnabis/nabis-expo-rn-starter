import React, { useContext } from 'react'
import { useQuery } from 'react-apollo-hooks'
// Graphql
import * as queries from '../../graphql/queries'
import { Me, User } from '../../graphql/types'
// Services
import { useSubscriptions } from '../Cache'
import { navigateTo } from '../Navigation'
// Auth
import { getAuthToken } from './AuthToken'

// Context
const MyContext = React.createContext<User.Fragment>(null)

// useMe Context
export const useMe = () => {
  return useContext(MyContext)
}

// withAuth wrapper
export const withAuth = (Component: React.ComponentType) => {
  return (props: any) => {
    if (!getAuthToken()) return navigateTo('Auth')

    // Validating against server
    const myResult = useQuery<Me.Query>(queries.me, { suspend: true })

    // Override TypeScript definition issue with the current version
    if (myResult.error) return navigateTo('Auth')

    useSubscriptions()

    return (
      <MyContext.Provider value={myResult.data.me}>
        <Component {...props} />
      </MyContext.Provider>
    )
  }
}
