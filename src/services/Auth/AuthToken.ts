import { AUTH_TOKEN } from '../../constants/Env'
import Storage from '../Storage'

let token: string | null

/**
 * Get Auth Token From Storage
 */
export async function getAuthToken() {
  if (token) return token

  const storedToken: string = await Storage.loadString(AUTH_TOKEN)

  token = storedToken

  return storedToken
}

/**
 * Store Auth Token
 * @param _token
 */
export async function storeAuthToken(_token: string) {
  await Storage.saveString(AUTH_TOKEN, _token)
  token = _token
  return
}

export function clearAuthToken() {
  Storage.remove(AUTH_TOKEN)
}
