import { navigateTo } from '../Navigation'
import Storage from '../Storage'
import { clearAuthToken } from './AuthToken'

export const signOut = () => {
  clearAuthToken()
  navigateTo('SignIn')
  return Storage.clear() // double clear everything in storage (just in case)
}
