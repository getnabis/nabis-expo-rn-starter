import * as firebase from 'firebase'

// Initialize Firebase
const config = {
  apiKey: '', // "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX",
  authDomain: '', // "XXXXXXXXXXXXXXX.firebaseapp.com",
  databaseURL: '', // "https://XXXXXXXXXXXXXXX.firebaseio.com",
  projectId: '', // "XXXXXXXXXXXXXXX",
  storageBucket: '', // "XXXXXXXXXXXXXXX.appspot.com",
  messagingSenderId: '', // "874376514949"
}

firebase.initializeApp(config)

export const database = firebase.database()
export const auth = firebase.auth()
export const provider = new firebase.auth.FacebookAuthProvider()
export const storage = firebase.storage()
