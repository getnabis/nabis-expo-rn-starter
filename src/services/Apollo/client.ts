import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloClient } from 'apollo-client'
import { from, split } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { onError } from 'apollo-link-error'
import { createHttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { OperationDefinitionNode } from 'graphql'

import { GRAPHQL_URL, IS_DEV, IS_SECURE } from '../../constants/Env'
import { getAuthToken } from '../Auth/AuthToken'
import Logger from '../Logger'
import { navigateTo } from '../Navigation'
import { InitialState, initialState } from './initialState'

// Graphql Endpoint
const httpUri = GRAPHQL_URL

// Websocket
const wsUri = IS_SECURE ? httpUri.replace(/^https?/, 'ws') : httpUri.replace(/^http?/, 'ws')

// Http Link
const httpLink = createHttpLink({ uri: httpUri })

const wsLink = new WebSocketLink({
  uri: wsUri,
  options: {
    reconnect: true,
    connectionParams: () => ({
      authToken: getAuthToken(),
    }),
  },
})

// Auth Link
const authLink = setContext(async (_, { headers }) => {
  const token = await getAuthToken()
  return token
    ? {
        headers: {
          ...headers,
          Authorization: `Bearer ${token}`,
        },
      }
    : { headers: { ...headers } }
})

// Error Link
const errorLink = onError(({ graphQLErrors, networkError, operation }) => {
  Logger.error('error afterware, graphQLErrors: ', graphQLErrors)

  if (networkError) {
    console.log('error afterware, networkError: ', networkError)

    return navigateTo('App')
  }

  if (operation.operationName === 'getCurrentUser') {
    console.log('just getting current user (getCurrentUser query)')
  }
})

const terminatingLink = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query) as OperationDefinitionNode
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  authLink.concat(httpLink),
)

// combine links
const link = from([authLink, errorLink, terminatingLink])

const cache = new InMemoryCache().restore(initialState)

export const client: ApolloClient<InitialState> = new ApolloClient({
  link,
  cache,
  connectToDevTools: IS_DEV,
  queryDeduplication: true, // TODO: check this
})
