import * as React from 'react'

export const withFirebaseService = (Component: any) => <T extends any>(props: T) => {
  // TODO: Need to update this to actually do something

  return (
    <React.Fragment>
      <Component {...props} />
    </React.Fragment>
  )
}
