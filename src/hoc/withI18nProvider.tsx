import * as React from 'react'
import { I18nextProvider } from 'react-i18next'

import { i18n } from '../i18n'

export const withI18nProvider = (Component: any) => (props: any) => (
  <I18nextProvider i18n={i18n}>
    <Component {...props} />
  </I18nextProvider>
)

// with custom i18nProvider
//
// import { I18nProvider } from '../context/I18nContext'
//
// export const withI18nProvider = (Component: any) => (props: any) => (
//   <I18nProvider>
//     <Component {...props} />
//   </I18nProvider>
// )
