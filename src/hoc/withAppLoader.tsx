import * as React from 'react'

import { AppLoading } from 'expo'

import { bootstrapAsync } from '../bootstrap'
import { useState } from '../hooks'
import Logger from '../services/Logger'

interface WithAppLoader {
  skipLoadingScreen?: boolean
}

export const withAppLoader = (Component: any) => <T extends WithAppLoader>({ skipLoadingScreen, ...props }: T) => {
  const [loading, setLoading] = useState(true)

  if (loading && !skipLoadingScreen) {
    return (
      <AppLoading
        startAsync={async () => {
          await bootstrapAsync()
        }}
        onError={(error: Error) => {
          Logger.warn(error)
        }}
        onFinish={() => {
          setLoading(false)
        }}
      />
    )
  }

  return <Component {...props} />
}
