import { fromRenderProps } from 'recompose'

import { ThemeContext, ThemeContextProps } from '../context/ThemeContext'

export const withThemeToggle = fromRenderProps(
  ThemeContext.Consumer,
  ({ toggle }: ThemeContextProps): Partial<ThemeContextProps> => ({ toggle }),
)
