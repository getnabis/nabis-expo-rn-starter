import * as React from 'react'

import { ActionSheetProvider, connectActionSheet } from '@expo/react-native-action-sheet'

export const withActionSheetProvider = (Component: any) =>
  connectActionSheet((props: any) => {
    return (
      <ActionSheetProvider>
        <Component {...props} />
      </ActionSheetProvider>
    )
  })
