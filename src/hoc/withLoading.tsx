import * as React from 'react'

import { LoadingView } from '../components'

export interface WithLoadingProps {
  loading?: boolean
}

export const withLoading = <P extends object>(Component: React.ComponentType<P>): React.FC<P & WithLoadingProps> => ({
  loading,
  ...props
}: WithLoadingProps) => (loading ? <LoadingView /> : <Component {...props as P} />)
