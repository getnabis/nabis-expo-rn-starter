import * as React from 'react'
import { DarkTheme, DefaultTheme, Provider } from 'react-native-paper'
import { ThemeMode } from '../system'

interface WithPaperProviderProps {
  themeMode: ThemeMode
}

export const withPaperProvider = <T extends WithPaperProviderProps>(Component: any) => (props: T) => (
  <Provider theme={props.themeMode === 'dark' ? DarkTheme : DefaultTheme}>
    <Component {...props} />
  </Provider>
)
