import * as React from 'react'
import FlashMessage from 'react-native-flash-message'

export const withFlashMessage = (Component: any) => <T extends any>(props: T) => (
  <React.Fragment>
    <Component {...props} />
    <FlashMessage position="top" />
  </React.Fragment>
)
