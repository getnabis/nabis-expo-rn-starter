import * as React from 'react'
import { AlertProvider } from '../context/AlertProvider'

export const withAlertProvider = (Component: any) => <T extends any>(props: T) => (
  <AlertProvider>
    <Component {...props} />
  </AlertProvider>
)
