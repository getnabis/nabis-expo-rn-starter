import * as React from 'react'

import { ApolloProvider } from 'react-apollo-hooks'
// import { ApolloProvider } from 'react-apollo'

import { client } from '../services/Apollo'

export const withApolloProvider = (Component: any) => (props: any) => (
  <ApolloProvider client={client}>
    <Component {...props} />
  </ApolloProvider>
)
