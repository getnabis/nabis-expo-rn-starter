import * as React from 'react'
import DropdownAlert, { DropdownAlertType } from 'react-native-dropdownalert'

type AlertWithTypeProps = (
  type: DropdownAlertType,
  title: string,
  message: string,
  payload?: object,
  interval?: number,
) => void

interface AlertContext {
  alertWithType: AlertWithTypeProps
}

const AlertContext = React.createContext<AlertContext>({
  alertWithType: () => {},
})

export class AlertProvider extends React.Component {
  dropdown: DropdownAlert

  render() {
    return (
      <AlertContext.Provider value={{ alertWithType: this.dropdown.alertWithType }}>
        {this.props.children}

        <DropdownAlert
          ref={(ref) => {
            this.dropdown = ref
          }}
        />
      </AlertContext.Provider>
    )
  }
}
