/*
 May not need as using react-i18n instead of creating maually
*/

// import * as React from 'react'
// import { I18nextProvider } from 'react-i18next'

// import Config from '../config'
// import { useAsyncStorage, useEffect, useState } from '../hooks'
// import { i18n, LanguageKey } from '../i18n'

// export interface I18nContextProps {
//   language: LanguageKey
//   changeLanguage: (language: LanguageKey) => void
// }

// export interface I18nState {
//   language: LanguageKey
//   hasLoadedCache: boolean
// }

// export const I18nContext = React.createContext<I18nContextProps>({
//   language: Config.i18n.fallbackLng as LanguageKey,
//   changeLanguage: () => {},
// })

// const initialState = {
//   language: Config.i18n.fallbackLng,
//   hasLoadedCache: false,
// } as I18nState

// export const I18nProvider: React.FC<any> = ({ children }) => {
//   const [languageState, setLanguageState] = useState<I18nState>(initialState)

//   useEffect(() => {
//     const { loadString } = useAsyncStorage('i18nLanguage')

//     if (!languageState.hasLoadedCache) {
//       loadString<LanguageKey>().then((language) => {
//         if (language !== null) {
//           setLanguageState({
//             ...languageState,
//             language,
//             hasLoadedCache: true,
//           })
//         }
//       })
//     }
//   })

//   useEffect(() => {
//     i18n.changeLanguage(languageState.language)
//   }, [languageState])

//   const { language } = languageState

//   const changeLanguage = (language: LanguageKey) => {
//     setLanguageState({ ...languageState, language })
//   }

//   return (
//     <I18nContext.Provider value={{ language, changeLanguage }}>
//       <I18nextProvider i18n={i18n}>{children}</I18nextProvider>
//     </I18nContext.Provider>
//   )
// }
