import * as Icon from '@expo/vector-icons'
import { Asset, Font } from 'expo'

export async function bootstrapAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/logo.png'),
      // ...
    ]),
    Font.loadAsync({
      ...Icon.Ionicons.font,
      'Lato-Black': require('./assets/fonts/Lato-Black.ttf'),
      'Lato-BlackItalic': require('./assets/fonts/Lato-BlackItalic.ttf'),
      'Lato-Bold': require('./assets/fonts/Lato-Bold.ttf'),
      'Lato-BoldItalic': require('./assets/fonts/Lato-BoldItalic.ttf'),
      'Lato-Hairline': require('./assets/fonts/Lato-Hairline.ttf'),
      'Lato-HairlineItalic': require('./assets/fonts/Lato-HairlineItalic.ttf'),
      'Lato-Italic': require('./assets/fonts/Lato-Italic.ttf'),
      'Lato-Light': require('./assets/fonts/Lato-Light.ttf'),
      'Lato-LightItalic': require('./assets/fonts/Lato-LightItalic.ttf'),
      'Lato-Regular': require('./assets/fonts/Lato-Regular.ttf'),
      'space-mono': require('./assets/fonts/SpaceMono-Regular.ttf'),
    }),
  ])
}
