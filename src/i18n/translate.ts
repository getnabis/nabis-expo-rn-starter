import { i18n } from './i18n'
import { LanguageKey, TranslationObject } from './types'

import idx from 'idx'

import * as locales from './locales'

const getResource = (language: LanguageKey): TranslationObject => locales[language]

export const translate = (accessor: (t: TranslationObject) => string | null) => {
  const { language } = i18n
  const resource = getResource(language as LanguageKey)
  return idx(resource, accessor)
}

export function translateWithVars(key: string, vars: object) {
  return key ? i18n.t(key, vars) : null
}
