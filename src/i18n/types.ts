import * as locales from './locales'

export type LanguageKey = keyof typeof locales

export type TranslationObject = typeof locales.en

export type ResourcesObjectType = Record<string, TranslationObject>
