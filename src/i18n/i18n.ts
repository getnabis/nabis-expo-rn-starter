import i18n from 'i18next'
import { reactI18nextModule } from '../hoc/node_modules/react-i18next'

import Config from '../config'
import LanguageDetector from './LanguageDetector'
import { en, es } from './locales'

i18n.use(LanguageDetector)

i18n.use(reactI18nextModule).init({
  fallbackLng: 'en',
  debug: Config.i18n.debug, // log
  interpolation: {
    escapeValue: false, // no need to escape for react as it does escape per default to prevent xss!
  },
  resources: {
    en,
    es,
  },
  // cache: {
  //   enabled: true
  // },
})

export { i18n }
