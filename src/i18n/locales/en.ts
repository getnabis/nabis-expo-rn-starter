export const en = {
  forms: {
    errors: {
      emailValid: 'Please enter a valid email address',
      passwordValid: 'Please enter a secure password',
      passwordConfirmValid: 'Please confirm your password',
      passwordConfirmMatch: 'Your passwords do not match',
      firstNameValid: 'Please enter your first name',
      lastNameValid: 'Please enter your last name',
      handleValid: 'Please enter a username',
    },
  },
  home: {
    title: 'Welcome',
    introduction: 'Thanks for using this app! We hope you like it!',
  },
  settings: {
    title: 'Settings',
    introduction: "You're on the Settings page.",
    options: {
      toggleSettings: 'Toggle Settings',
    },
  },
}
