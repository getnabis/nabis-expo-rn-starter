export const es = {
  forms: {
    errors: {
      emailValid: 'Por favor, introduce una dirección de correo electrónico válida',
      passwordValid: 'Por favor ingrese una contraseña segura',
      passwordConfirmValid: 'Por favor, confirme su contraseña',
      passwordConfirmMatch: 'Tus contraseñas no coinciden',
      firstNameValid: 'Por favor, introduzca su nombre de pila',
      lastNameValid: 'Por favor ingrese su apellido',
      handleValid: 'por favor, ingrese un nombre de usuario',
    },
  },
  home: {
    title: 'Bienvenido',
    introduction: 'Gracias por usar esta aplicación! ¡Esperamos que os guste!',
  },
  settings: {
    title: 'Ajustes',
    introduction: 'Estás en la página de configuración.',
    options: {
      toggleSettings: 'Configuración de alternar',
    },
  },
}
