import * as React from 'react'
import { Animated } from 'react-native'

interface Props {}
interface State {}

export class FadeInView extends React.Component<Props, State> {
  state = {
    fadeAnim: new Animated.Value(0),
  }

  componentDidMount() {
    Animated.timing(this.state.fadeAnim, {
      toValue: 1,
      duration: 2000,
    }).start()
  }

  render() {
    return <Animated.View style={{ opacity: this.state.fadeAnim }}>{this.props.children}</Animated.View>
  }
}
