import * as React from 'react'
import { createDrawerNavigator, DrawerItems, DrawerItemsProps, SafeAreaView, ScrollView } from 'react-navigation'

import { Avatar, Icon, IconType } from '../components'
import { Box, Small, ThemedProps, withTheme } from '../system'
import { MainTabNavigator } from './MainTabNavigator'

interface Props extends ThemedProps, DrawerItemsProps {}

const Drawer = withTheme(({ theme, ...props }: Props) => (
  <SafeAreaView style={{ flex: 1 }} forceInset={{ top: 'always', horizontal: 'never' }}>
    <Box row space="between" px={2} py={1} borderColor="shade8" borderBottomWidth={0.5}>
      <Box flex={0.3}>
        <Avatar size={40} borderRadius={20} source={{ uri: 'http://i.pravatar.cc/100' }} />
      </Box>
      <Box flex={1} center>
        <Small>Cali Framework</Small>
        <Small>Made with ❤️ by SiteAppy</Small>
      </Box>
    </Box>
    <ScrollView>
      <DrawerItems {...props} />
    </ScrollView>
  </SafeAreaView>
))

const MenuIcon = ({ name, type, focused }: { name: string; type: IconType; focused: boolean }) => (
  <Icon type={type} name={name} size={24} color={focused ? 'shade0' : 'shade2'} />
)

const DrawerNavigator = createDrawerNavigator(
  {
    Home: {
      screen: MainTabNavigator,
      navigationOptions: {
        drawerLabel: 'Components',
        drawerIcon: (props: any) => <MenuIcon type="feather" name="bell" focused={props.focused} />,
      },
    },
  },
  {
    contentComponent: Drawer,
    contentOptions: {
      activeTintColor: 'accent1',
      activeBackgroundColor: 'shade6',
      inactiveTintColor: 'shade7',
      inactiveBackgroundColor: 'shade9',
      style: {},
      labelStyle: {
        fontSize: 15,
        fontWeight: 'normal',
        fontFamily: 'Lato',
        color: 'accent2',
        marginLeft: 12,
      },
    },
  },
)

export default DrawerNavigator
