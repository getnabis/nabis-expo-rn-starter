import React from 'react'
import { NativeRouter, Route, Switch } from 'react-router-native'

import Login from '../routes/Login'
import NewProduct from '../routes/NewProduct'
import Products from '../routes/Products'
import Signup from '../routes/Signup'
import AuthLoading from '../screens/AuthLoadingScreen'

export default () => (
  <NativeRouter>
    <Switch>
      <Route exact path="/" component={AuthLoading} />
      <Route exact path="/login" component={Login} />
      <Route exact path="/signup" component={Signup} />
      <Route exact path="/products" component={Products} />
      <Route exact path="/new-product" component={NewProduct} />
    </Switch>
  </NativeRouter>
)
