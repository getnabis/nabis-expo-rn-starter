export const Routes = {
  App: 'ROOT_APP',
  Auth: 'ROOT_AUTH',
  Tabs: 'TABS',
  TabsHome: 'TABS_HOME',
  TabsLinks: 'TABS_LINKS',
  TabsSettings: 'TABS_SETTINGS',
  TabsBookmarks: 'TABS_BOOKMARKS',
  TabsLikes: 'TABS_LIKES',
  TabsPrivate: 'TABS_PRIVATE',
  TabsProfile: 'TABS_PROFILE',
  OtherScreen: 'OTHER_SCREEN',
  ForgotPassword: 'AUTH_FORGOT_PASSWORD',
  SignUp: 'AUTH_SIGN_UP',
  SignIn: 'AUTH_SIGN_IN',
  Settings: 'SETTINGS',
}

export type RouteName = keyof typeof Routes
