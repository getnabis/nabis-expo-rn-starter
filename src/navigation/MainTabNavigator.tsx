import { FontAwesome as Icon } from '@expo/vector-icons'
import React from 'react'
import { View } from 'react-native'
import { createBottomTabNavigator, NavigationScreenProps } from 'react-navigation'
// import {  createDrawerNavigator } from 'react-navigation'
// import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs'

import { makeTabBarIcon } from '../components/TabBarIcon'
import { MultiBar, MultiBarToggle } from '../modules/react-native-multibar'
import createStackNavigator from './createStackNavigator'
import { Routes } from './Routes'

import BookmarksScreen from '../screens/BookmarksScreen'
import HomeScreen from '../screens/HomeScreen'
import LikesScreen from '../screens/LikesScreen'
import ProfileScreen from '../screens/ProfileScreen'
// import SettingsScreen from '../screens/SettingsScreen'

// import { DrawerComponent } from '../components/DrawerComponent'
// import { Platform } from 'react-native'

const HomeStack = createStackNavigator(
  {
    Home: HomeScreen,
  },
  {
    navigationOptions: () => ({
      tabBarLabel: 'Home',
      tabBarIcon: makeTabBarIcon('home', 'feather'),
    }),
  },
)

// const createTabNavigator = Platform.select({
//   default: createBottomTabNavigator,
//   android: createMaterialBottomTabNavigator,
// })

const MultiBarScreen = () => <View />

export const MainTabNavigator = createBottomTabNavigator(
  {
    [Routes.TabsHome]: HomeStack,
    [Routes.TabsBookmarks]: {
      screen: BookmarksScreen,
      navigationOptions: () => ({
        tabBarLabel: 'Bookmarks',
        tabBarIcon: makeTabBarIcon('bookmark'),
      }),
    },
    MultiBar: {
      screen: MultiBarScreen,
      navigationOptions: ({ navigation }: NavigationScreenProps) => ({
        tabBarLabel: ' ',
        tabBarIcon: () => (
          <MultiBarToggle
            navigation={navigation}
            actionSize={30}
            icon={<Icon name="plus" color="#FFFFFF" size={24} />}
            routes={[
              {
                routeName: Routes.OtherScreen,
                color: '#FF8360',
                icon: <Icon name="rocket" color="#333333" size={15} />,
              },
              {
                routeName: Routes.OtherScreen,
                color: '#E8E288',
                icon: <Icon name="dashboard" color="#333333" size={15} />,
              },
              {
                routeName: Routes.OtherScreen,
                color: '#7DCE82',
                icon: <Icon name="gears" color="#333333" size={15} />,
              },
            ]}
          />
        ),
      }),
      params: {
        navigationDisabled: true,
      },
    },
    [Routes.TabsLikes]: {
      screen: LikesScreen,
      navigationOptions: () => ({
        tabBarLabel: 'Likes',
        tabBarIcon: makeTabBarIcon('heart'),
      }),
    },
    [Routes.TabsProfile]: {
      screen: ProfileScreen,
      navigationOptions: () => ({
        tabBarLabel: 'Profile',
        tabBarIcon: makeTabBarIcon('user'),
      }),
    },
  },
  {
    tabBarComponent: MultiBar, // optional
    tabBarOptions: {
      showLabel: false,
      activeTintColor: '#F8F8F8',
      inactiveTintColor: '#586589',
      style: {
        backgroundColor: '#171F33',
      },
      tabStyle: {},
    },
  },
)

// const AppNavigator = createStackNavigator(
//   {
//     [Routes.Tabs]: TabsNavigator,
//     [Routes.OtherScreen]: SettingsScreen,
//   },
//   {
//     headerMode: 'none',
//   },
// )

// const DrawerNavigator = createDrawerNavigator(
//   {
//     AppTabs: AppNavigator,
//   },
//   {
//     contentComponent: DrawerComponent,
//   },
// )

// export default DrawerNavigator

export default MainTabNavigator
