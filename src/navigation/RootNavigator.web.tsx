import { createBrowserApp } from '@react-navigation/web'

import { AppNavigator } from './AppNavigator'

export const RootNavigator = createBrowserApp(AppNavigator)
