import { createStackNavigator } from 'react-navigation'
import { fromBottom } from 'react-navigation-transitions'

import ForgotPasswordScreen from '../screens/ForgotPasswordScreen'
import SignInScreen from '../screens/SignInScreen'
import SignUpScreen from '../screens/SignUpScreen'

import { Routes } from './Routes'

const ModalStack = createStackNavigator(
  {
    [Routes.ForgotPassword]: ForgotPasswordScreen,
  },
  {
    mode: 'modal',
    headerMode: 'none',
    defaultNavigationOptions: {
      gesturesEnabled: false,
    },
    transitionConfig: () => fromBottom(),
  },
)

ModalStack.displayName = 'ModalStack'

const AuthNavigator = createStackNavigator(
  {
    [Routes.SignIn]: SignInScreen,
    [Routes.SignUp]: SignUpScreen,
    ModalStack,
  },
  {
    headerMode: 'none',
  },
)

AuthNavigator.displayName = 'AuthNavigator'

export default AuthNavigator
