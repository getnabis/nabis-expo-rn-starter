import { NavigationScreenProp, NavigationState } from 'react-navigation'
import { Theme } from '../system'

export const getNavOptions = (theme: Theme) => ({
  headerStyle: theme.styles.NavHeaderStyle,
  headerTitleStyle: theme.styles.NavHeaderTitle,
  headerTintColor: theme.colors.accent1,
})

export interface DefaultNavOptions {
  navigation: NavigationScreenProp<NavigationState>
  screenProps: { theme: Theme }
}
