import { createSwitchNavigator } from 'react-navigation'
import AuthLoadingScreen from '../screens/AuthLoadingScreen'
// import AuthNavigator from './AuthNavigator'
// import MainTabNavigator from './MainTabNavigator'

export const AppNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    // Auth: AuthNavigator,
    // App: MainTabNavigator,
  },
  {
    initialRouteName: 'AuthLoading',
  },
)
