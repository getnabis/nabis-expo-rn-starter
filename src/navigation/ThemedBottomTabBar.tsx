import * as React from 'react'
import { BottomTabBar, BottomTabBarProps } from 'react-navigation'

import { ThemedProps, withTheme } from '../system'

interface Props extends BottomTabBarProps, ThemedProps {}

export const ThemedBottomTabBar = withTheme(({ theme, ...props }: Props) => (
  <BottomTabBar
    {...props}
    activeTintColor={theme.colors.accent1}
    inactiveTintColor={theme.colors.shade6}
    style={theme.styles.BottomTabBar}
  />
))
