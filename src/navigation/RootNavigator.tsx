import { createAppContainer } from 'react-navigation'
import { AppNavigator } from './AppNavigator'

export const RootNavigator = createAppContainer(AppNavigator)

RootNavigator.displayName = 'RootNavigator'
