import { Color } from '../system'

declare module 'styled-system' {
  interface ColorProps {
    backgroundColor?: Color
    bg?: Color
    color?: Color
  }

  interface BackgroundProps {
    backgroundColor?: Color
  }

  interface TextColorProps {
    color?: Color
  }

  interface BordersProps {
    borderColor?: Color
  }

  interface BorderColorProps {
    borderColor?: Color
  }
}
