export const MultiBarColors = {
  activeTintColor: '#FAFAFA',
  inactiveTintColor: '#9B9B9B',
  toggleColor: '#1DA2FF',
}
