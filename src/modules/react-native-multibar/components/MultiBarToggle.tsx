import * as React from 'react'
import { Animated, Route, StyleSheet, TouchableOpacity, Vibration, View } from 'react-native'

import { NavigationScreenProps } from 'react-navigation'
import { MultiBarColors } from '../utils'

const DEFAULT_TOGGLE_SIZE = 80
const DEFAULT_ACTION_SIZE = 40
const DEFAULT_TOGGLE_ANIMATION_DURATION = 300
const DEFAULT_ACTION_STAGING_DURATION = 100
const DEFAULT_ACTION_ANIMATION_DURATION = 200
const DEFAULT_NAVIGATION_DELAY = 500
const DEFAULT_EXPANDING_ANGLE = 135

const PATTERN = [1000, 2000, 3000]

type Routes = Route[]

interface Props extends NavigationScreenProps {
  routes: Routes
  actionSize: number
  actionExpandingAngle: number
  toggleColor: string
  toggleSize: number
  navigationDelay: number
  toggleAnimationDuration: number
  actionAnimationDuration: number
  actionStagingDuration: number
  animateIcon: boolean
  actionVibration?: boolean
  icon?: React.ReactElement
  toggleVibration?: boolean
}

type Activations = Record<string, Animated.Value>

interface State {
  measured: boolean
  active: boolean
  activations: Activations
}

export class MultiBarToggle extends React.Component<Props, State> {
  static defaultProps: Props = {
    routes: [],
    actionSize: DEFAULT_ACTION_SIZE,
    actionExpandingAngle: DEFAULT_EXPANDING_ANGLE,
    toggleColor: MultiBarColors.toggleColor,
    toggleSize: DEFAULT_TOGGLE_SIZE,
    navigationDelay: DEFAULT_NAVIGATION_DELAY,
    toggleAnimationDuration: DEFAULT_TOGGLE_ANIMATION_DURATION,
    actionAnimationDuration: DEFAULT_ACTION_ANIMATION_DURATION,
    actionStagingDuration: DEFAULT_ACTION_STAGING_DURATION,
    animateIcon: true,
    navigation: null,
  }

  activation = new Animated.Value(0)

  state: State = {
    measured: false,
    active: false,
    activations: {},
  }

  actionPressed = (route: Route) => {
    this.togglePressed()

    const { actionVibration, navigationDelay } = this.props

    actionVibration && Vibration.vibrate(PATTERN)

    if (route.routeName) {
      setTimeout(
        () =>
          this.props.navigation.navigate({
            routeName: route.routeName,
          }),
        navigationDelay,
      )
    }

    route.onPress && route.onPress()
  }

  togglePressed = () => {
    const {
      routes,
      toggleVibration,
      toggleAnimationDuration,
      actionAnimationDuration,
      actionStagingDuration,
      animateIcon,
    } = this.props

    if (this.state.active) {
      this.setState({ active: false })

      const animations = []

      if (animateIcon) {
        animations.push(Animated.timing(this.activation, { toValue: 0, duration: toggleAnimationDuration }))
      }

      Animated.parallel([
        ...animations,
        Animated.stagger(
          actionStagingDuration,
          routes.map((_, i) =>
            Animated.timing(this.state.activations[`actionActivation_${routes.length - 1 - i}`], {
              toValue: 0,
              duration: actionAnimationDuration,
            }),
          ),
        ),
      ]).start()
    } else {
      this.setState({ active: true })

      const animations = []

      if (animateIcon) {
        animations.push(Animated.timing(this.activation, { toValue: 1, duration: toggleAnimationDuration }))
      }

      Animated.parallel([
        ...animations,
        Animated.stagger(
          actionStagingDuration,
          routes.map((_, i) =>
            Animated.timing(this.state.activations[`actionActivation_${i}`], {
              toValue: 1,
              duration: actionAnimationDuration,
            }),
          ),
        ),
      ]).start()
    }

    toggleVibration && Vibration.vibrate(PATTERN)
  }

  renderActions = () => {
    const { routes, actionSize, actionExpandingAngle } = this.props

    const STEP = actionExpandingAngle / routes.length

    return routes.map((route, i) => {
      const offset = (STEP * (i + 1)) / DEFAULT_EXPANDING_ANGLE - 0.5
      const angle = -90 + DEFAULT_EXPANDING_ANGLE * offset - STEP / 2
      const radius = 80

      const x = radius * Math.cos((-angle * Math.PI) / 180)
      const y = radius * Math.sin((-angle * Math.PI) / 180)

      const activationScale = this.state.activations[`actionActivation_${i}`].interpolate({
        inputRange: [0, 0.5, 1],
        outputRange: [0, 0, 1],
      })

      const activationPositionX = this.state.activations[`actionActivation_${i}`].interpolate({
        inputRange: [0, 1],
        outputRange: [0, x],
      })

      const activationPositionY = this.state.activations[`actionActivation_${i}`].interpolate({
        inputRange: [0, 1],
        outputRange: [0, y],
      })

      const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity)

      return (
        <Animated.View
          key={`action_${i}`}
          style={[
            Styles.actionContainer,
            {
              marginLeft: -actionSize / 2,
              left: activationPositionX,
              bottom: activationPositionY,
              transform: [{ scale: activationScale }],
            },
          ]}
        >
          <AnimatedTouchable
            style={[
              Styles.actionContent,
              {
                width: actionSize,
                height: actionSize,
                borderRadius: actionSize / 2,
                backgroundColor: route.color,
              },
            ]}
            onPress={() => this.actionPressed(route)}
          >
            {route.icon}
          </AnimatedTouchable>
        </Animated.View>
      )
    })
  }

  /**
   * Create animation values for each action.
   */
  makeActivations = (routes: Routes) => {
    const activations: Record<string, Animated.Value> = {}
    routes.forEach((_v, i) => (activations[`actionActivation_${i}`] = new Animated.Value(0)))

    this.setState({ measured: true, activations })
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.routes !== this.props.routes) {
      this.makeActivations(nextProps.routes)
    }
  }

  componentDidMount() {
    this.makeActivations(this.props.routes)
  }

  render() {
    const { icon, toggleColor, toggleSize } = this.props

    const activationRotate = this.activation.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '135deg'],
    })

    const activationScale = this.activation.interpolate({
      inputRange: [0, 0.5, 1],
      outputRange: [1, 1.25, 1],
    })

    const AnimatedTouchable = Animated.createAnimatedComponent(TouchableOpacity)

    return (
      <View pointerEvents="box-none" style={Styles.container}>
        {this.state.measured && <View style={Styles.actionsWrapper}>{this.renderActions()}</View>}
        <AnimatedTouchable onPress={this.togglePressed} activeOpacity={1}>
          <Animated.View
            style={[
              Styles.toggleButton,
              {
                transform: [{ rotate: activationRotate }, { scale: activationScale }],
                width: toggleSize,
                height: toggleSize,
                borderRadius: toggleSize / 2,
                backgroundColor: toggleColor,
              },
            ]}
          >
            {icon}
          </Animated.View>
        </AnimatedTouchable>
      </View>
    )
  }
}

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  toggleButton: {
    top: 15,
    left: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  toggleIcon: {
    fontSize: 30,
    color: 'white',
  },
  actionsWrapper: {
    position: 'absolute',
    bottom: 0,
  },
  actionContainer: {
    position: 'absolute',
  },
  actionContent: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
