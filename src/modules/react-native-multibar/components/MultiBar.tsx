import * as React from 'react'
import { StyleProp, StyleSheet, TouchableOpacity, View, ViewProps } from 'react-native'
import { NavigationRoute, NavigationScreenProps, SafeAreaView } from 'react-navigation'

import { MultiBarColors } from '../utils'

type RenderIconProp = ({}) => React.ReactChildren

interface MultiBarProps extends NavigationScreenProps {
  style: StyleProp<ViewProps>
  renderIcon: RenderIconProp
  jumpTo: (key: string) => void
  activeTintColor: string
  inactiveTintColor: string
}

export const MultiBar: React.FC<MultiBarProps> = ({
  style,
  navigation,
  activeTintColor = MultiBarColors.activeTintColor,
  inactiveTintColor = MultiBarColors.inactiveTintColor,
  renderIcon,
  jumpTo,
}) => {
  const { index, routes } = navigation.state

  return (
    <SafeAreaView pointerEvents="box-none" style={Styles.container} forceInset={{ top: 'never', bottom: 'always' }}>
      <SafeAreaView style={[Styles.fakeBackground, style]} forceInset={{ top: 'never', bottom: 'always' }}>
        <View style={{ height: 49 }} />
      </SafeAreaView>
      <View pointerEvents="box-none" style={Styles.content}>
        {routes.map((route, _index) => {
          const focused = index === _index

          if (!route.params || !route.params.navigationDisabled) {
            return (
              <TabIcon
                key={route.key}
                route={route}
                renderIcon={renderIcon}
                focused={focused}
                activeTintColor={activeTintColor}
                inactiveTintColor={inactiveTintColor}
                onPress={() => (!route.params || !route.params.navigationDisabled) && jumpTo(route.key)}
              />
            )
          }

          const Icon = renderIcon({
            route,
            focused,
            tintColor: focused ? activeTintColor : inactiveTintColor,
          })

          return {
            ...Icon,
            key: 'simple',
          }
        })}
      </View>
    </SafeAreaView>
  )
}

const TabIcon: React.FC<TabIconProps> = ({
  route,
  renderIcon,
  focused,
  activeTintColor,
  inactiveTintColor,
  onPress,
}) => (
  <TouchableOpacity style={Styles.tabStyle} onPress={() => onPress && onPress()}>
    {renderIcon({
      route,
      focused,
      tintColor: focused ? activeTintColor : inactiveTintColor,
    })}
  </TouchableOpacity>
)

interface TabIconProps {
  route: NavigationRoute
  renderIcon: RenderIconProp
  activeTintColor: string
  inactiveTintColor: string
  focused: boolean
  onPress: () => void
}

const Styles = StyleSheet.create({
  container: {
    position: 'absolute',
    bottom: 0,
    width: '100%',
    justifyContent: 'flex-end',
    minHeight: 160,
  },
  fakeBackground: {
    position: 'absolute',
    width: '100%',
  },
  content: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  tabStyle: {
    height: 50,
    width: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
})
