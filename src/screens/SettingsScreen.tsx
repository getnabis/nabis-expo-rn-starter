import * as React from 'react'
import { View } from 'react-native'
import { FlatList } from 'react-navigation'

import { DefaultNavOptions, getNavOptions } from '../navigation/getNavOptions'
import { Screen, Text } from '../system'

const data = new Array(150).fill(0)

export default class SettingsScreen extends React.Component {
  static navigationOptions = ({ screenProps }: DefaultNavOptions) => ({
    title: 'Settings',
    ...getNavOptions(screenProps.theme),
  })

  renderItem = ({ index }: any) => {
    return (
      <View style={{ height: 50, padding: 12 }}>
        <Text>Item {index}</Text>
      </View>
    )
  }

  keyExtractor = (_: any, index: number): string => {
    return `${index}`
  }

  render() {
    return (
      <Screen scrollable>
        <FlatList data={data} keyExtractor={this.keyExtractor} renderItem={this.renderItem} />
      </Screen>
    )
  }
}
