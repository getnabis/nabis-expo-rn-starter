import * as React from 'react'
import { NavigationScreenProp } from 'react-navigation'

import { Screen, Text } from '../system'

export interface ProfileProps extends NavigationScreenProp {}
export interface ProfileState {}

export default class ProfileScreen extends React.Component<ProfileProps, ProfileState> {
  state = {}

  render() {
    return (
      <Screen center middle>
        <Text>Profile Screen</Text>
      </Screen>
    )
  }
}
