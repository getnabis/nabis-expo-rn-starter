import * as React from 'react'
import { NavigationScreenProp } from 'react-navigation'

import { Screen, Text } from '../system'

interface LikesProps extends NavigationScreenProp {}

interface LikesState {}

export default class LikesScreen extends React.Component<LikesProps, LikesState> {
  state = {}

  render() {
    return (
      <Screen center middle>
        <Text>Likes Screen</Text>
      </Screen>
    )
  }
}
