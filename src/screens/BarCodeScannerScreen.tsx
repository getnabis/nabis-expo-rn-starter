import * as React from 'react'

import { BarCodeScanner, Permissions } from 'expo'
import { StyleSheet } from 'react-native'
import { NavigationEvents, NavigationScreenProp } from 'react-navigation'

import { TextButton } from '../components'
import { closeScreen } from '../services/Navigation'
import { Box, Text, ThemedProps } from '../system'

interface Props extends ThemedProps, NavigationScreenProp {}

interface State {
  isPermissionsGranted: boolean
  type: typeof BarCodeScanner.Constants.Type.back
}

export default class BarcodeScannerExample extends React.Component<Props, State> {
  static navigationOptions = {
    title: '<BarCodeScanner />',
  }

  state = {
    isPermissionsGranted: false,
    type: BarCodeScanner.Constants.Type.back,
  }

  componentDidFocus = async () => {
    const { status } = await Permissions.askAsync(Permissions.CAMERA)
    this.setState({ isPermissionsGranted: status === 'granted' })
  }

  render() {
    if (!this.state.isPermissionsGranted) {
      return (
        <Box flex={1}>
          <NavigationEvents onDidFocus={this.componentDidFocus} />
          <Text>You have not granted permission to use the camera on this device!</Text>
        </Box>
      )
    }
    return (
      <Box flex={1}>
        <BarCodeScanner
          onBarCodeScanned={this.handleBarCodeScanned}
          barCodeTypes={[BarCodeScanner.Constants.BarCodeType.qr, BarCodeScanner.Constants.BarCodeType.pdf417]}
          type={this.state.type === 'back' ? 'back' : 'front'}
          style={StyleSheet.absoluteFillObject}
        />
        <Box key="toolbar" position="absolute" p={2} space="between" bg="white20">
          <TextButton color="white" title="Toggle Direction" onPress={this.toggleType} />
        </Box>
      </Box>
    )
  }

  toggleType = () =>
    this.setState({
      type:
        this.state.type === BarCodeScanner.Constants.Type.back
          ? BarCodeScanner.Constants.Type.front
          : BarCodeScanner.Constants.Type.back,
    })

  handleBarCodeScanned = (data: any) => {
    closeScreen()
    requestAnimationFrame(() => {
      alert(JSON.stringify(data))
    })
  }
}
