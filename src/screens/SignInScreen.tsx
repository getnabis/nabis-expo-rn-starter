import * as React from 'react'

import { BigRoundedButton, TextButton } from '../components'
import { navigateTo } from '../services/Navigation'
import { Box, Col, Flex, Grid, H1, Row, Screen, Text } from '../system/components'

const SignInScreen = () => {
  return (
    <Screen safearea>
      <Flex mx={3}>
        <Grid>
          <Row />
          <Row>
            <Col>
              <H1>Sign In</H1>
              <Text>Let's get you into the app!</Text>
            </Col>
          </Row>
          <Box my={2}>
            <Box my={1}>
              <BigRoundedButton onPress={() => navigateTo('SignIn')} title="Sign In" />
            </Box>
            <Box my={1}>
              <BigRoundedButton size="large" onPress={() => navigateTo('SignUp')} title="Sign Up" />
            </Box>
          </Box>
          <Box center>
            <TextButton onPress={() => navigateTo('ForgotPassword')} title="Forgot Password?" />
          </Box>
          <Row />
        </Grid>
      </Flex>
    </Screen>
  )
}

SignInScreen.navigationOptions = {
  title: 'Sign In',
  headerMode: 'none',
}

export default SignInScreen
