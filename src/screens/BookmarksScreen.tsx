import * as React from 'react'
import { FlatList, TouchableOpacity } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'

import { Screen, Text } from '../system'

import { randomColor } from '../utils/randomColor'

export interface BookmarksProps extends NavigationScreenProp {}

export interface BookmarksState {}

export default class BookmarksScreen extends React.Component<BookmarksProps, BookmarksState> {
  state = {}

  renderCard = () => (
    <TouchableOpacity
      style={{
        marginTop: 10,
        marginHorizontal: 10,
        flex: 1,
        minHeight: 120,
        borderRadius: 10,
        backgroundColor: randomColor(),
      }}
    />
  )

  render() {
    return (
      <Screen safearea>
        <Text>Bookmarks</Text>
        <FlatList data={[...new Array(9)].map((_, i) => ({ key: `item_${i}` }))} renderItem={this.renderCard} />
      </Screen>
    )
  }
}
