import * as React from 'react'

import { AsyncStorage } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'

import { BigRoundedButton, MenuButton, ThemeSwitchButton } from '../components'
import { DefaultNavOptions, getNavOptions } from '../navigation/getNavOptions'
import { H1, Screen } from '../system'

interface HomeProps extends NavigationScreenProp {}
interface HomeState {}

export default class HomeScreen extends React.Component<HomeProps, HomeState> {
  static navigationOptions = ({ screenProps }: DefaultNavOptions) => ({
    title: 'Home',
    headerLeft: MenuButton,
    ...getNavOptions(screenProps.theme),
  })

  signOutAsync = async () => {
    await AsyncStorage.clear()
    this.props.navigation.navigate('Auth')
  }

  render() {
    return (
      <Screen center middle>
        <H1>Home</H1>
        <ThemeSwitchButton />
        <BigRoundedButton title="Sign Out" onPress={this.signOutAsync} />
      </Screen>
    )
  }
}
