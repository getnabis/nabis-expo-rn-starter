import * as React from 'react'
import { NavigationScreenProp } from 'react-navigation'

import { H1, Screen, Text } from '../system'

export interface ForgotPasswordProps extends NavigationScreenProp {}
export interface ForgotPasswordState {}

export default class ForgotPasswordScreen extends React.Component<ForgotPasswordProps, ForgotPasswordState> {
  state = {}

  render() {
    return (
      <Screen safearea>
        <H1>Forgot Password</H1>
        <Text>To reset your password, please fill in your email address.</Text>
      </Screen>
    )
  }
}
