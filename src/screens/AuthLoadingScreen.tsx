import * as React from 'react'
import { View } from 'react-native'
import { NavigationScreenProp } from 'react-navigation'

// import { Spinner } from '../components'
// import { AUTH_TOKEN } from '../constants/Env'
// import { navigateTo } from '../services/Navigation'
// import { Screen } from '../system'

export interface AuthLoadingProps extends NavigationScreenProp {}

export default class AuthLoadingScreen extends React.Component<AuthLoadingProps> {
  componentWillMount() {
    // this.asyncLoad()
  }

  // asyncLoad = () => {
  //   const authToken = AsyncStorage.getItem(AUTH_TOKEN)
  //   navigateTo(authToken ? 'App' : 'Auth')
  // }

  // render() {
  //   return (
  //     <Screen center middle>
  //       <Spinner color="shade0" />
  //     </Screen>
  //   )
  // }

  render() {
    return <View style={{ flex: 1 }} />
  }
}
