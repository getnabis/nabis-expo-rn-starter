import { auth, User } from 'firebase'
import { useEffect } from './universal-react-hooks'
import { useLoadingValue } from './useLoadingValue'

export interface FirebaseAuthStateHook {
  user?: firebase.User
  initialising: boolean
}

export const useFirebaseAuth = (auth: auth.Auth): FirebaseAuthStateHook => {
  const { loading, setValue, value } = useLoadingValue<User>(() => auth.currentUser)

  useEffect(() => {
    const listener = auth.onAuthStateChanged(setValue)

    return () => {
      listener()
    }
  }, [auth])

  return {
    initialising: loading,
    user: value,
  }
}
