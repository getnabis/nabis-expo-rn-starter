import { useState } from './universal-react-hooks'

export const useSimpleForm = <T extends {}>(initialValues: T, callback: () => void) => {
  const [inputs, setInputs] = useState<T>(initialValues)

  const handleSubmit = () => {
    callback()
  }

  const handleInputChange = (inputName: keyof T) => (value: any) => {
    setInputs((_inputs) => ({
      ..._inputs,
      [inputName]: value,
    }))
  }

  return {
    handleSubmit,
    handleInputChange,
    inputs,
  }
}
