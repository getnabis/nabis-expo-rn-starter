// @ts-ignore
import { DangerZone } from 'expo'
const { DeviceMotion } = DangerZone

import { useEffect, useState } from './universal-react-hooks'

export const useDeviceMotion = () => {
  const [motion, setMotion] = useState<
    Pick<DeviceMotionEvent, 'acceleration' | 'accelerationIncludingGravity' | 'rotationRate' | 'interval'>
  >({
    acceleration: {
      x: null,
      y: null,
      z: null,
    },
    accelerationIncludingGravity: {
      x: null,
      y: null,
      z: null,
    },
    rotationRate: {
      alpha: null,
      beta: null,
      gamma: null,
    },
    interval: 0,
  })

  useEffect(() => {
    const handle = (deviceMotionEvent: DeviceMotionEvent) => {
      console.log(deviceMotionEvent)
      setMotion(deviceMotionEvent)
    }

    DeviceMotion.addListener(handle)

    return () => {
      DeviceMotion.removeListener(handle)
    }
  }, [])

  return motion
}
