// @ts-ignore
import { DangerZone } from 'expo'
const { Lottie: LottieView } = DangerZone as any

export { LottieView }
