export function randomData() {
  return [...new Array(9)].map((_: any, i: number) => ({ key: `item_${i}`, value: `${i}` }))
}
