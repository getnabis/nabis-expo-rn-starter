import { Platform } from 'react-native'
import { isIphoneX } from 'react-native-iphone-x-helper'
import { Header } from 'react-navigation'

export function getHeaderHeight() {
  const NOTCH_HEIGHT = isIphoneX() ? 25 : 0

  const BASE_HEADER_HEIGHT = Header.HEIGHT

  const HEADER_HEIGHT = Platform.OS === 'ios' ? BASE_HEADER_HEIGHT + NOTCH_HEIGHT : BASE_HEADER_HEIGHT + 20

  return HEADER_HEIGHT
}

export function getHeaderInset(): any {
  const HEADER_HEIGHT = getHeaderHeight()
  return Platform.select({
    android: {
      contentContainerStyle: {
        paddingTop: HEADER_HEIGHT,
      },
    },
    ios: {
      contentInset: { top: HEADER_HEIGHT },
      contentOffset: { y: -HEADER_HEIGHT },
    },
  })
}
