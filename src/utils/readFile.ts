import RNFetchBlob from 'react-native-fetch-blob'

export const readFileAsync = async (filePath: string) =>
  RNFetchBlob.fs.readFile(filePath, 'base64').then(data => new Buffer(data, 'base64'))
