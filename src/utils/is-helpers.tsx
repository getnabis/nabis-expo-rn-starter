export const isDef = (n: any) => n !== undefined && n !== null
export const isTrue = (n: any) => n === true || isDef(n)
export const isNot = (n: any) => !isTrue(n)
export const isElse = <T, X, Y>(n: T, x: X, y?: Y) => (isTrue(n) ? (isDef(y) ? y : x) : n)
export const is = <T, X>(n: T, x: X) => isElse<T, X | T, null>(n, x || n, undefined)
