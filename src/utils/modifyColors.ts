import tinycolor from 'tinycolor2'

export const lighten = (color: string, amount: number = 40) =>
  tinycolor(color)
    .lighten(amount)
    .toString()

export const darken = (color: string, amount: number = 40) =>
  tinycolor(color)
    .darken(amount)
    .toString()

export const opacity = (color: string, alpha: number = 1) =>
  tinycolor(color)
    .setAlpha(alpha)
    .toString()
