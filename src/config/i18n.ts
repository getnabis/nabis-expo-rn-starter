import { IS_DEV } from '../constants/Env'

export default {
  enabled: true,
  langPickerRender: true,
  langList: ['en', 'us'], // ['en-US', 'es-ES'],
  fallbackLng: 'en', // 'en-US',
  cookie: 'lang',
  debug: IS_DEV, // enables logging for i18n
}
