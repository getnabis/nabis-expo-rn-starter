import i18n from './i18n'

const Config = {
  i18n,
}

export default Config
