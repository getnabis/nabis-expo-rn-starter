export const AUTH_TOKEN = 'AUTH_TOKEN'
export const THEME_STATE = 'THEME_STATE'

export const GRAPHQL_URL = process.env.GRAPHQL_URL || 'http://localhost:4000/graphql'
export const IS_DEV = process.env.NODE_ENV !== 'production'
export const IS_SECURE = process.env.SECURE

export const PLATFORM = typeof document !== 'undefined' ? 'web' : 'mobile'
